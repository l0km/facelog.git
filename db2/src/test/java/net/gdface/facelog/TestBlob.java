package net.gdface.facelog;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.Test;

public class TestBlob {
    public static final String URL = "jdbc:mysql://192.168.10.160:3306/test?characterEncoding=utf8&useInformationSchema=true";
    public static final String USER = "root";
    public static final String PASSWORD = "root";

	@Test
	public void test() {
		Connection conn = null;
		try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(URL, USER, PASSWORD);
            String sql = "SELECT feature FROM fl_feature WHERE md5=?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setObject(1, "051e2c4174229e3079318a5678258c8b");
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
            	byte[] array = rs.getObject(1, byte[].class);
            	if(null != array){
            		System.out.println("array leng=" + array.length);
            	}else{
            		System.out.println("array is null");
            	}
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
