package net.gdface.facelog;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.ImmutableMap;

import gu.sql2java.BaseBean;

public class BaseBeanSupport {
	protected BaseBeanSupport() {
	}
	/**
	 * 更新bean的 {@code jsonFieldID}指定字段(JSON)
	 * @param bean
	 * @param jsonFieldID 字段ID
	 * @param jsonProps 包含多个字段的JSON字符串
	 * @return bean always
	 */
	public static <B extends BaseBean> B updateJsonField(B bean,int jsonFieldID,String jsonProps){
		if( null != bean && jsonProps != null){
			JSONObject input = JSON.parseObject(jsonProps, JSONObject.class);
			JSONObject dst;
			
			if(bean.getValue(jsonFieldID) != null){
				dst = JSON.parseObject((String)bean.getValue(jsonFieldID), JSONObject.class).fluentPutAll(input);
			}else{
				dst = input;
			}
			bean.setValue(jsonFieldID, JSON.toJSONString(dst));
		}
		return bean;
	}
	/**
	 * 更新bean的 {@code jsonFieldID}指定字段(JSON)
	 * @param bean
	 * @param jsonFieldID 字段ID
	 * @param key JSON字段名
	 * @param value JSON字段值
	 * @return bean always
	 */
	public static <B extends BaseBean> B  updateJsonField(B bean,int jsonFieldID,String key,String value){
		if( null !=bean && key != null){
			if(bean.getValue(jsonFieldID) != null){
				JSONObject dst = JSON.parseObject((String)bean.getValue(jsonFieldID), JSONObject.class).fluentPut(key, value);
				bean.setValue(jsonFieldID, JSON.toJSONString(dst));
			}else if(null != value){
				bean.setValue(jsonFieldID, JSON.toJSONString(ImmutableMap.of(key,value)));
			}
		}
		return bean;
	}
}
