package net.gdface.facelog;

import net.gdface.facelog.db.Constant;
import net.gdface.facelog.db.DeviceBean;

public class DeviceBeanSupport extends BaseBeanSupport implements Constant {
	private DeviceBeanSupport() {
	}
	/**
	 * 更新{@link DeviceBean}的 options字段(JSON)
	 * @param deviceBean
	 * @param jsonProps 包含多个字段的JSON字符串
	 * @return deviceBean always
	 */
	public static DeviceBean updateOptions(DeviceBean deviceBean,String jsonProps){
		return updateJsonField(deviceBean,FL_DEVICE_ID_OPTIONS,jsonProps);
	}
	/**
	 * 更新{@link DeviceBean}的 options字段(JSON)
	 * @param deviceBean
	 * @param key JSON字段名
	 * @param value JSON字段值
	 * @return deviceBean always
	 */
	public static DeviceBean updateOptions(DeviceBean deviceBean,String key,String value){
		return updateJsonField(deviceBean, FL_DEVICE_ID_OPTIONS, key, value);
	}
}
