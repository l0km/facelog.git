package net.gdface.facelog;

import java.util.List;
import java.util.Map;
import gu.sql2java.StringMatchType;
import net.gdface.annotation.CodegenInvalidValue;
import net.gdface.annotation.DeriveMethod;
import net.gdface.facelog.db.DeviceBean;
import net.gdface.facelog.db.DeviceGroupBean;
import net.gdface.facelog.db.ErrorLogBean;
import net.gdface.facelog.db.FaceBean;
import net.gdface.facelog.db.FeatureBean;
import net.gdface.facelog.db.ImageBean;
import net.gdface.facelog.db.LogBean;
import net.gdface.facelog.db.LogLightBean;
import net.gdface.facelog.db.PermitBean;
import net.gdface.facelog.db.PersonBean;
import net.gdface.facelog.db.PersonGroupBean;

/**
 * FaceLog 服务接口<br>
 * <ul>
 * <li>所有标明为图像数据的参数,是指具有特定图像格式的图像数据(如jpg,png...),而非无格式的原始点阵位图</li>
 * <li>所有{@link RuntimeException}异常会被封装在{@code ServiceRuntimeException}抛出,
 * client端可以通过{@code ServiceRuntimeException#getType()}获取异常类型.<br>
 * 异常类型定义参见{@link net.gdface.facelog.CommonConstant.ExceptionType},<br>
 * 例如: 在执行涉及数据库操作的异常{@code RuntimeDaoException}，
 * 被封装到{@code ServiceRuntimeException}抛出时type为{@link net.gdface.facelog.CommonConstant.ExceptionType#DAO}</li>
 * <li>所有数据库对象(Java Bean,比如 {@link PersonBean}),在执行保存操作(save)时,
 * 如果为新增记录({@link PersonBean#isNew()}为true),则执行insert操作,否则执行update操作,
 * 如果数据库已经存在指定的记录而{@code isNew()}为{@code true},则那么执行insert操作数据库就会抛出异常，
 * 所以请在执行save时特别注意{@code isNew()}状态</li>
 * <li>对于以add为前缀的添加记录方法,在添加记录前会检查数据库中是否有(主键)相同记录,
 * 如果有则会抛出异常{@code DuplicateRecordException}</li>
 * <li>所有带{@link Token}参数的方法都需要提供访问令牌,访问令牌分为人员令牌,设备令牌和root令牌(仅用于root帐户),
 * 注释中标注为{@code PERSON_ONLY}的方法只接受人员令牌,
 * 注释中标注为{@code DEVICE_ONLY}的方法只接受设备令牌,
 * 注释中标注为{@code ROOT_ONLY}的方法只接受root令牌,
 * 关于令牌申请和释放参见{@link net.gdface.facelog.IFaceLog#applyPersonToken(int, String, boolean)},{@link net.gdface.facelog.IFaceLog#releasePersonToken(Token)},{@link net.gdface.facelog.IFaceLog#online(DeviceBean)},{@link net.gdface.facelog.IFaceLog#offline(Token)}</li>
 * </ul>
 * @author guyadong
 */
public interface IFaceLog{

	/**
	 * 返回personId指定的人员记录(脱敏数据)
	 * @param personId
	 * @return 没有找到匹配的记录则返回{@code null}
	 */
	public PersonBean getPerson(int personId) ;

	/**
	 * 返回 list 指定的人员记录(脱敏数据)
	 * @param idList 人员id列表
	 * @return 没有找到匹配的记录则返回空表
	 */
	public List<PersonBean> getPersons(List<Integer> idList);

	/**
	 * 根据证件号码返回人员记录(脱敏数据)
	 * @param papersNum
	 * @return 没有找到匹配的记录则返回{@code null}
	 */
	public PersonBean getPersonByPapersNum(String papersNum);

	/**
	 * 根据证件号码返回人员记录
	 * @param papersNum
	 * @param token 访问令牌
	 * @return 没有找到匹配的记录则返回{@code null}
	 */
	@DeriveMethod(methodSuffix="Safe")
	public PersonBean getPersonByPapersNum(String papersNum, Token token);

	/**
	 * 根据登记的手机号码返回人员记录(脱敏数据)
	 * @param mobilePhone
	 * @return 没有找到匹配的记录则返回{@code null}
	 */
	public PersonBean getPersonByMobilePhone(String mobilePhone);
	/**
	 * 根据登记的手机号码返回人员记录
	 * @param mobilePhone
	 * @param token 访问令牌
	 * @return 没有找到匹配的记录则返回{@code null}
	 */
	@DeriveMethod(methodSuffix="Real")
	public PersonBean getPersonByMobilePhone(String mobilePhone, Token token);

	/**
	 * 返回personId指定的人员记录
	 * @param personId
	 * @param token 访问令牌
	 * @return 没有找到匹配的记录则返回{@code null}
	 */
	@DeriveMethod(methodSuffix="Real")
	public PersonBean getPerson(int personId, Token token);
	/**
	 * 返回 list 指定的人员记录
	 * @param idList 人员id列表
	 * @param token 访问令牌
	 * @return 没有找到匹配的记录则返回空表
	 */
	@DeriveMethod(methodSuffix="Real")
	public List<PersonBean> getPersons(List<Integer> idList, Token token);
	/**
	 * 返回在指定设备上允许通行的所有人员记录<br>
	 * @param deviceId 设备ID
	 * @param ignoreSchedule 是否忽略时间过滤器(fl_permit.schedule字段)的限制
	 * @param excludePersonIds 要排除的人员记录id,可为{@code null}
	 * @param timestamp 不为{@code null}时返回fl_person.expiry_date大于指定时间戳的所有fl_person记录
	 * @return 返回的用户对象列表中，过滤所有有效期失效的用户<br>
	 */
	public List<Integer> getPersonsPermittedOnDevice(int deviceId, boolean ignoreSchedule, List<Integer> excludePersonIds, Long timestamp);
	
	/**
	 * 返回在指定设备上允许通行的所有人员ID列表<br>
	 * @param deviceId 设备ID
	 * @param ignoreSchedule 是否忽略时间过滤器(fl_permit.schedule字段)的限制
	 * @param excludePersonIds 要排除的人员记录id,可为{@code null}
	 * @param timestamp 不为{@code null}时返回fl_person.expiry_date大于指定时间戳的所有fl_person记录
	 * @return 以用户组为单位返回用户ID列表,每条记录的格式为 用户组ID:用户ID列表(,分隔),比如 '42:307,229,15' 代表用户组42下的用户ID列表'307,229,15'<br>
	 * 				 过滤所有有效期失效的用户 
	 * @since 4.2.1
	 */
	public List<String> getPersonsPermittedOnDeviceByGroup(int deviceId, boolean ignoreSchedule, List<Integer> excludePersonIds, Long timestamp);

	/**
	 * 返回在指定设备上允许通行的所有用户数据包<br>
	 * @param deviceId 设备ID
	 * @param ignoreSchedule 是否忽略时间过滤器(fl_permit.schedule字段)的限制
	 * @param excludePersonIds 要排除的人员记录id,可为{@code null}
	 * @param timestamp 不为{@code null}时返回fl_person.expiry_date大于指定时间戳的所有fl_person记录
	 * @param sdkVersion 算法(SDK)版本号
	 * @return 返回的用户数据包列表中，过滤所有有效期失效的用户<br>
	 * @since 4.2.0
	 */
	public List<PersonDataPackage> loadPersonDataPackagesPermittedOnDevice(int deviceId, boolean ignoreSchedule, List<Integer> excludePersonIds, Long timestamp, String sdkVersion);
	/**
	 * 返回可在指定设备上通过的用户ID列表对应的用户数据包
	 * @param personIds
	 * @param sdkVersion 算法(SDK)版本号
	 * @param deviceId 设备ID
	 * @return 返回的用户数据包列表中,过滤所有有效期失效和无通行权限的用户<br>
	 * @since 4.2.0
	 */
	public List<PersonDataPackage> loadPersonDataPackages(List<Integer> personIds, String sdkVersion, int deviceId);

	/**
	 * 返回可在指定设备上通过的(同一组)用户ID列表对应的用户数据包
	 * @param personIds 在同一用户组的用户ID列表,使用列表中第一个用户记录来计算通行权限
	 * @param sdkVersion 算法(SDK)版本号
	 * @param deviceId 设备ID
	 * @return 返回的用户数据包列表中,过滤所有有效期失效和无通行权限的用户<br>
	 * @since 4.2.1
	 */
	public List<PersonDataPackage> loadPersonDataPackagesInSameGroup(List<Integer> personIds, String sdkVersion, int deviceId);
	/**
	 * 删除personId指定的人员(person)记录及关联的所有记录
	 * <br>{@code PERSON_ONLY}
	 * @param personId
	 * @param token 访问令牌
	 * @return 删除成功返回1,否则返回0
	 */
	public int deletePerson(int personId, Token token);

	/**
	 * 删除personIdList指定的人员(person)记录及关联的所有记录
	 * <br>{@code PERSON_ONLY}
	 * @param personIdList 人员id列表
	 * @param token 访问令牌
	 * @return 返回删除的 person 记录数量
	 */
	public int deletePersons(List<Integer> personIdList, Token token);

	/**
	 * 删除papersNum指定的人员(person)记录及关联的所有记录
	 * <br>{@code PERSON_ONLY}
	 * @param papersNum 证件号码
	 * @param token 访问令牌
	 * @return 返回删除的 person 记录数量
	 * @see #deletePerson(int, Token)
	 */
	public int deletePersonByPapersNum(String papersNum, Token token);

	/**
	 * 删除papersNum指定的人员(person)记录及关联的所有记录
	 * <br>{@code PERSON_ONLY}
	 * @param papersNumList 证件号码列表
	 * @param token 访问令牌
	 * @return 返回删除的 person 记录数量
	 */
	public int deletePersonsByPapersNum(List<String> papersNumList, Token token);
	
	/**
	 * 删除mobilePhone指定的人员(person)记录及关联的所有记录
	 * <br>{@code PERSON_ONLY}
	 * @param mobilePhone 电话号码
	 * @param token 访问令牌
	 * @return 返回删除的 person 记录数量
	 * @see #deletePerson(int, Token)
	 * @since 5.0.0
	 */
	public int deletePersonByMobilePhone(final String mobilePhone, Token token);
	
	/**
	 * 删除mobilePhoneList指定的人员(person)记录及关联的所有记录
	 * <br>{@code PERSON_ONLY}
	 * @param mobilePhoneList 电话号码列表
	 * @param token 访问令牌
	 * @return 返回删除的 person 记录数量
	 * @since 5.0.0
	 */
	public int deletePersonsByMobilePhone(final List<String> mobilePhoneList, Token token);

	/**
	 * 判断是否存在personId指定的人员记录
	 * @param persionId
	 * @return true if exists,otherwise false
	 */
	public boolean existsPerson(int persionId);

	/**
	 * 判断 personId 指定的人员记录是否过期
	 * @param personId
	 * @return true if disabled,otherwise false
	 */
	public boolean isDisable(int personId);

	/**
	 * 设置 personId 指定的人员为禁止状态<br>
	 * 将{@code fl_person.expiry_date}设置为昨天
	 * <br>{@code PERSON_ONLY}
	 * @param personId 
	 * @param moveToGroupId 将用户移动到指定的用户组，为{@code null}则不移动
	 * @param deletePhoto 为{@code true}删除用户标准照
	 * @param deleteFeature 为{@code true}删除用户所有的人脸特征数据(包括照片)
	 * @param deleteLog 为{@code true}删除用户所有通行日志
	 * @param token 访问令牌
	 */
	public void disablePerson(int personId, Integer moveToGroupId, boolean deletePhoto, boolean deleteFeature, boolean deleteLog, Token token);

	/**
	 * 修改 personId 指定的人员记录的有效期
	 * <br>{@code PERSON_ONLY}
	 * @param personId
	 * @param expiryDate 失效日期
	 * @param token 访问令牌
	 */
	public void setPersonExpiryDate(int personId, long expiryDate, Token token) ;

	/**
	 * 修改 personId 指定的人员记录的有效期
	 * <br>{@code PERSON_ONLY}
	 * @param personId
	 * @param expiryDate 失效日期,{@code yyyy-MM-dd}或{@code yyyy-MM-dd HH:mm:ss}或{@code yyyy-MM-dd'T'HH:mm:ss.SSSZ}(ISO8601)格式日期字符串
	 * @param token 访问令牌
	 */
	@DeriveMethod(methodSuffix="TimeStr")
	public void setPersonExpiryDate(int personId, String expiryDate, Token token);

	/**
	 * 修改 personIdList 指定的人员记录的有效期
	 * <br>{@code PERSON_ONLY}
	 * @param personIdList 人员id列表
	 * @param expiryDate 失效日期 
	 * @param token 访问令牌
	 */
	@DeriveMethod(methodSuffix="List")
	public void setPersonExpiryDate(List<Integer> personIdList, long expiryDate, Token token);

	/**
	 * 设置 personIdList 指定的人员为禁止状态
	 * <br>{@code PERSON_ONLY}
	 * @param personIdList 人员id列表
	 * @param token 访问令牌
	 */
	@DeriveMethod(methodSuffix="List")
	public void disablePerson(List<Integer> personIdList, Token token);

	/**
	 * 返回 persionId 关联的所有日志记录
	 * @param personId fl_person.id
	 * @return 日志记录列表
	 * @deprecated 
	 */
	public List<LogBean> getLogBeansByPersonId(int personId);

	/**
	 * 返回 persionId 关联的所有日志记录(只返回访问令牌允许访问的记录)
	 * @param personId fl_person.id
     * @param token 访问令牌
	 * @return 日志记录列表
	 * @since 5.0.0
	 */
	@DeriveMethod(methodSuffix="Safe")
	public List<LogBean> getLogBeansByPersonId(int personId, Token token);
	/**
	 * 返回所有人员记录
	 * @return 返回 fl_person.id 列表
	 * @deprecated
	 */
	public List<Integer> loadAllPerson();
	
	/**
	 * 返回所有人员记录(只返回访问令牌允许访问的记录)
     * @param token 访问令牌
	 * @return 返回 fl_person.id 列表
	 * @since 5.0.0
	 */
	@DeriveMethod(methodSuffix="Safe")
	public List<Integer> loadAllPerson(Token token);

	/**
	 * 返回 where 指定的所有人员记录
	 * @param where 'WHERE'开头的SQL条件语句,为{@code null}或空时加载所有记录
	 * @return 返回 fl_person.id 列表
	 * @deprecated
	 */
	public List<Integer> loadPersonIdByWhere(String where);
	
	/**
	 * 返回 where 指定的所有人员记录(只返回访问令牌允许访问的记录)
	 * @param where 'WHERE'开头的SQL条件语句,为{@code null}或空时加载所有记录
     * @param token 访问令牌
	 * @return 返回 fl_person.id 列表
	 * @since 5.0.0
	 */
	@DeriveMethod(methodSuffix="Safe")
	public List<Integer> loadPersonIdByWhere(String where, Token token);
	/**
	 * 返回 where 指定的所有人员记录(脱敏数据)
	 * @param where 'WHERE'开头的SQL条件语句,为{@code null}或空时加载所有记录
	 * @param startRow 记录起始行号 (first row = 1, last row = -1)
	 * @param numRows 返回记录条数 为负值是返回{@code startRow}开始的所有行
	 * @return 人员记录列表,没有找到匹配的记录则返回空表
	 * @deprecated
	 */
	public List<PersonBean> loadPersonByWhere(String where, int startRow, int numRows);
	/**
	 * 返回 where 指定的所有人员记录
	 * @param where 'WHERE'开头的SQL条件语句,为{@code null}或空时加载所有记录
	 * @param startRow 记录起始行号 (first row = 1, last row = -1)
	 * @param numRows 返回记录条数 为负值是返回{@code startRow}开始的所有行
	 * @param token 访问令牌
	 * @return 人员记录列表,没有找到匹配的记录则返回空表
	 * @since 5.0.0
	 */
	@DeriveMethod(methodSuffix="Real")
	public List<PersonBean> loadPersonByWhere(String where, int startRow, int numRows, Token token);

	/**
	 * 返回满足{@code where}条件的日志记录(fl_person)数目
	 * @param where 'WHERE'开头的SQL条件语句,为{@code null}时返回所有记录
	 * @return 返回满足{@code where}条件的日志记录(fl_person)数目
	 * @deprecated
	 */
	public int countPersonByWhere(String where);
	/**
	 * 返回满足{@code where}条件的日志记录(fl_person)数目(只统计访问令牌允许访问的记录)
	 * @param where 'WHERE'开头的SQL条件语句,为{@code null}时返回所有记录
     * @param token 访问令牌
	 * @return 返回满足{@code where}条件的日志记录(fl_person)数目
	 * @since 5.0.0
	 */	
	@DeriveMethod(methodSuffix="Safe")
	public int countPersonByWhere(String where, Token token);
	/**
	 * 保存人员(person)记录
	 * @param personBean {@code fl_person}表记录
	 * @param token 访问令牌
	 * @return 保存的{@link PersonBean}
	 */
	public PersonBean savePerson(PersonBean personBean, Token token);

	/**
	 * 保存人员(person)记录
	 * <br>{@code PERSON_ONLY}
	 * @param persons {@code fl_person}表记录
	 * @param token 访问令牌
	 */
	public void savePersons(List<PersonBean> persons, Token token);

	/**
	 * 保存人员信息记录
	 * @param personBean {@code fl_person}表记录
	 * @param idPhoto 标准照图像对象,可为null
	 * @param token 访问令牌
	 * @return 保存的{@link PersonBean}
	 */
	@DeriveMethod(methodSuffix="WithPhoto")
	public PersonBean savePerson(PersonBean personBean, byte[] idPhoto, Token token);
	
	/**
	 * 保存人员信息记录<br>
	 * 该方法与{@code savePersonWithPhoto(PersonBean personBean, byte[] idPhoto, Token token)}不同,
	 * 该方法支持对人脸照片提取特征并保存到数据库,
	 * 但需要服务运行时有人脸识别算法支持，否则会抛出异常:FaceApiRuntimeException: 'NOT AVAILABLE FaceApi INSTANCE'
	 * @param personBean {@code fl_person}表记录
	 * @param idPhoto 标准照图像对象,可为null
	 * @param exractFeature 是否需要使用人脸识别算法提取并保存标准照图像人脸特征,idPhoto为null时忽略
	 * @param token 访问令牌
	 * @return 保存的{@link PersonBean}
	 * @since 4.2.0
	 */
	@DeriveMethod(methodSuffix="WithPhotoAndExtractFeature")
	public PersonBean savePerson(PersonBean personBean, byte[] idPhoto, boolean exractFeature, Token token);
	
	/**
	 * 保存人员信息记录(包含标准照)<br>
	 * 每一张照片对应一个{@code PersonBean}记录, {@code photos}元素不可重复
	 * {@code photos}与{@code persons}列表一一对应
	 * {@code PERSON_ONLY}
	 * @param photos 照片列表 
	 * @param persons 人员记录对象列表
	 * @param token 访问令牌
	 * @return 保存的{@link PersonBean}记录条数
	 */
	@DeriveMethod(methodSuffix="WithPhoto")
	public int savePersons(List<byte[]> photos, List<PersonBean> persons, Token token);

	/**
	 * 保存人员信息记录
	 * @param personBean {@code fl_person}表记录
	 * @param idPhotoMd5 标准照图像对象,可为null
	 * @param featureMd5 用于验证的人脸特征数据对象,可为null
	 * @param token 访问令牌
	 * @return 保存的{@link PersonBean}
	 */
	@DeriveMethod(methodSuffix="WithPhotoAndFeatureSaved")
	public PersonBean savePerson(PersonBean personBean, String idPhotoMd5, String featureMd5, Token token);

	/**
	 * 保存人员信息记录<br>
	 * 适用于单张人脸提取特征算法
	 * @param personBean {@code fl_person}表记录
	 * @param idPhoto 标准照图像,可为null
	 * @param featureBean 用于验证的人脸特征数据对象,可为null
	 * @param token 访问令牌
	 * @return 保存的{@link PersonBean}
	 */
	@DeriveMethod(methodSuffix="WithPhotoAndFeature")
	public PersonBean savePerson(PersonBean personBean, byte[] idPhoto, FeatureBean featureBean, Token token);

	/**
	 * 保存人员信息记录<br>
	 * 适用于多张人脸合成特征算法
	 * @param personBean {@code fl_person}表记录
	 * @param idPhoto 标准照图像,可为null
	 * @param feature 用于验证的人脸特征数据,不可重复, 参见 {@link #addFeature(byte[], String, Integer, List, String, Token)}
	 * @param featureVersion 特征(SDk)版本号
	 * @param faceBeans 可为{@code null},参见 {@link #addFeature(byte[], String, Integer, List, String, Token)}
	 * @param token 访问令牌
	 * @return 保存的{@link PersonBean}
	 */
	@DeriveMethod(methodSuffix="WithPhotoAndFeatureMultiFaces")
	public PersonBean savePerson(PersonBean personBean, byte[] idPhoto, byte[] feature, String featureVersion, List<FaceBean> faceBeans, Token token);

	/**
	 * 保存人员信息记录<br>
	 * 适用于多张人脸合成特征算法<br>
	 * {@code photos}与{@code faces}为提取特征{@code feature}的人脸照片对应的人脸位置对象，必须一一对应,
	 * 该方法用于多张照片合成一个人脸特征的算法
	 * @param personBean {@code fl_person}表记录
	 * @param idPhoto 标准照图像,可为null
	 * @param feature 用于验证的人脸特征数据 
	 * @param featureVersion 特征(SDk)版本号
	 * @param photos 检测到人脸的照片列表
	 * @param faces 检测人脸信息列表
	 * @param token (设备)访问令牌
	 * @return 保存的{@link PersonBean}对象
	 */
	@DeriveMethod(methodSuffix="WithPhotoAndFeatureMultiImage")
	public PersonBean savePerson(PersonBean personBean, byte[] idPhoto, byte[] feature, String featureVersion,
			List<byte[]> photos, List<FaceBean> faces, Token token);

	/**
	 * 保存人员信息记录<br>
	 * 适用于单张人脸提取特征算法
	 * @param personBean 人员信息对象,{@code fl_person}表记录
	 * @param idPhoto 标准照图像,可以为{@code null}
	 * @param feature 人脸特征数据,可以为{@code null}
	 * @param featureVersion 特征(SDk)版本号
	 * @param featureImage 提取特征源图像,为null 时,默认使用idPhoto
	 * @param faceBean 人脸位置对象,为null 时,不保存人脸数据,忽略featureImage
	 * @param token (设备)访问令牌
	 * @return 保存的{@link PersonBean}
	 */
	@DeriveMethod(methodSuffix="Full")
	public PersonBean savePerson(PersonBean personBean, byte[] idPhoto, byte[] feature, String featureVersion,
			byte[] featureImage, FaceBean faceBean, Token token);

	/**
	 * 替换personId指定的人员记录的人脸特征数据,同时删除原特征数据记录(fl_feature)及关联的fl_face表记录
	 * @param personId 人员记录id,{@code fl_person.id}
	 * @param featureMd5 人脸特征数据记录id (已经保存在数据库中)
	 * @param deleteOldFeatureImage 是否删除原特征数据记录间接关联的原始图像记录(fl_image)
	 * @param token 访问令牌
	 */
	public void replaceFeature(int personId, String featureMd5, boolean deleteOldFeatureImage, Token token);
	/**
	 * (主动更新机制实现)返回fl_person.update_time字段大于指定时间戳( {@code timestamp} )的所有fl_person记录<br>
	 * 同时包含fl_feature更新记录引用的fl_person记录<br>
	 * @param timestamp
	 * @return 返回fl_person.id 列表
	 * @deprecated
	 */
	public List<Integer> loadUpdatedPersons(long timestamp);
	/**
	 * (主动更新机制实现)返回fl_person.update_time字段大于指定时间戳( {@code timestamp} )的所有fl_person记录<br>
	 * (只返回访问令牌允许访问的记录)<br>
	 * 同时包含fl_feature更新记录引用的fl_person记录<br>
	 * @param timestamp
	 * @param token 访问令牌
	 * @return 返回fl_person.id 列表
	 * @since 5.0.0
	 */
	@DeriveMethod(methodSuffix="Safe")
	public List<Integer> loadUpdatedPersons(long timestamp, Token token);
	/**
	 * (主动更新机制实现)返回fl_person.update_time字段大于指定时间戳( {@code timestamp} )的所有fl_person记录<br>
	 * 同时包含fl_feature更新记录引用的fl_person记录<br>
	 * @param timestamp 时间戳,{@code yyyy-MM-dd}或{@code yyyy-MM-dd HH:mm:ss}或{@code yyyy-MM-dd'T'HH:mm:ss.SSSZ}(ISO8601)格式日期字符串
	 * @return 返回fl_person.id 列表
	 * @deprecated
	 */
	@DeriveMethod(methodSuffix="Timestr")
	public List<Integer> loadUpdatedPersons(String timestamp);
	/**
	 * (主动更新机制实现)返回fl_person.update_time字段大于指定时间戳( {@code timestamp} )的所有fl_person记录<br>
	 * (只返回访问令牌允许访问的记录)<br>
	 * 同时包含fl_feature更新记录引用的fl_person记录<br>
	 * @param timestamp 时间戳,{@code yyyy-MM-dd}或{@code yyyy-MM-dd HH:mm:ss}或{@code yyyy-MM-dd'T'HH:mm:ss.SSSZ}(ISO8601)格式日期字符串
	 * @param token 访问令牌
	 * @return 返回fl_person.id 列表
	 * @since 5.0.0
	 */
	@DeriveMethod(methodSuffix="TimestrSafe")
	public List<Integer> loadUpdatedPersons(String timestamp, Token token);
	
	/**
	 * (主动更新机制实现)返回 fl_person.update_time 字段大于指定时间戳( {@code timestamp} )的所有fl_person记录<br>
	 * @param timestamp
	 * @return 返回fl_person.id 列表
	 * @deprecated
	 */
	public List<Integer> loadPersonIdByUpdateTime(long timestamp);
	/**
	 * (主动更新机制实现)返回 fl_person.update_time 字段大于指定时间戳( {@code timestamp} )的所有fl_person记录<br>
	 * (只返回访问令牌允许访问的记录)
	 * @param timestamp
	 * @return 返回fl_person.id 列表
	 * @since 5.0.0
	 */
	@DeriveMethod(methodSuffix="Safe")
	public List<Integer> loadPersonIdByUpdateTime(long timestamp, Token token);
	/**
	 * (主动更新机制实现)返回 fl_person.update_time 字段大于指定时间戳( {@code timestamp} )的所有fl_person记录<br>
	 * @param timestamp 时间戳,{@code yyyy-MM-dd}或{@code yyyy-MM-dd HH:mm:ss}或{@code yyyy-MM-dd'T'HH:mm:ss.SSSZ}(ISO8601)格式日期字符串
	 * @return 返回fl_person.id 列表
	 * @deprecated
	 */
	@DeriveMethod(methodSuffix="TimeStr")
	public List<Integer> loadPersonIdByUpdateTime(String timestamp);
	
	/**
	 * (主动更新机制实现)返回 fl_person.update_time 字段大于指定时间戳( {@code timestamp} )的所有fl_person记录<br>
	 * (只返回访问令牌允许访问的记录)<br>
	 * @param timestamp 时间戳,{@code yyyy-MM-dd}或{@code yyyy-MM-dd HH:mm:ss}或{@code yyyy-MM-dd'T'HH:mm:ss.SSSZ}(ISO8601)格式日期字符串
	 * @param token 访问令牌
	 * @return 返回fl_person.id 列表
	 * @since 5.0.0
	 */
	@DeriveMethod(methodSuffix="TimeStrSafe")
	public List<Integer> loadPersonIdByUpdateTime(String timestamp, Token token);

	/**
	 * (主动更新机制实现)返回 fl_feature.update_time 字段大于指定时间戳( {@code timestamp} )的所有fl_feature记录<br>
	 * @param timestamp
	 * @return 返回 fl_feature.md5 列表
	 * @deprecated
	 */
	public List<String> loadFeatureMd5ByUpdate(long timestamp);
	/**
	 * (主动更新机制实现)返回 fl_feature.update_time 字段大于指定时间戳( {@code timestamp} )的所有fl_feature记录<br>
	 * (只返回访问令牌允许访问的记录)<br>
	 * @param timestamp
	 * @param token 访问令牌
	 * @return 返回 fl_feature.md5 列表
	 * @since 5.0.0
	 */
	@DeriveMethod(methodSuffix="Safe")
	public List<String> loadFeatureMd5ByUpdate(long timestamp, Token token);

	/**
	 * (主动更新机制实现)返回 fl_feature.update_time 字段大于指定时间戳( {@code timestamp} )的所有fl_feature记录<br>
	 * @param timestamp 时间戳,{@code yyyy-MM-dd}或{@code yyyy-MM-dd HH:mm:ss}或{@code yyyy-MM-dd'T'HH:mm:ss.SSSZ}(ISO8601)格式日期字符串
	 * @return 返回 fl_feature.md5 列表
	 * @deprecated
	 */
	@DeriveMethod(methodSuffix="TimeStr")
	public List<String> loadFeatureMd5ByUpdate(String timestamp);
	
	/**
	 * (主动更新机制实现)返回 fl_feature.update_time 字段大于指定时间戳( {@code timestamp} )的所有fl_feature记录<br>
	 * (只返回访问令牌允许访问的记录)
	 * @param timestamp 时间戳,{@code yyyy-MM-dd}或{@code yyyy-MM-dd HH:mm:ss}或{@code yyyy-MM-dd'T'HH:mm:ss.SSSZ}(ISO8601)格式日期字符串
	 * @param token 访问令牌
	 * @return 返回 fl_feature.md5 列表
	 * @since 5.0.0
	 */
	@DeriveMethod(methodSuffix="TimeStrSafe")
	public List<String> loadFeatureMd5ByUpdate(String timestamp, Token token);
	/**
	 * 添加一条验证日志记录
	 * <br>{@code DEVICE_ONLY}
	 * @param logBean 日志记录对象
	 * @param token 访问令牌
	 * @throws DuplicateRecordException 数据库中存在相同记录
	 */
	public void addLog(LogBean logBean, Token token) throws DuplicateRecordException;

	/**
	 * 添加一条验证日志记录
	 * <br>{@code DEVICE_ONLY}
	 * {@code faceBean}和{@code featureImage}必须全不为{@code null},否则抛出异常
	 * @param logBean 日志记录对象
	 * @param faceBean 用于保存到数据库的提取人脸特征的人脸信息对象
	 * @param featureImage 用于保存到数据库的现场采集人脸特征的照片
	 * @param token 访问令牌
	 * @throws DuplicateRecordException 数据库中存在相同记录
	 */
	@DeriveMethod(methodSuffix="Full")
	public void addLog(final LogBean logBean, final FaceBean faceBean, final byte[] featureImage, Token token) throws DuplicateRecordException;
	
	/**
	 * 添加一组验证日志记录(事务存储)
	 * <br>{@code DEVICE_ONLY}
	 * @param logBeans
	 * @param token 访问令牌
	 * @throws DuplicateRecordException 数据库中存在相同记录
	 */
	public void addLogs(List<LogBean> logBeans, Token token) throws DuplicateRecordException;
	/**
	 * 添加一组验证日志记录(事务存储)<br>
	 * 所有输入参数的list长度必须一致(不能有{@code null})元素,每3个相同索引位置元素为一组关联的日志记录，参见{@link #addLog(LogBean, FaceBean, byte[], Token)}
	 * @param logBeans 日志记录对象
	 * @param faceBeans 为用于保存到数据库的提取人脸特征的人脸信息对象
	 * @param featureImages 用于保存到数据库的现场采集人脸特征的照片
	 * @param token 访问令牌
	 * @throws DuplicateRecordException 数据库中存在相同记录
	 */
	@DeriveMethod(methodSuffix="Full")
	void addLogs(final List<LogBean> logBeans, final List<FaceBean> faceBeans, final List<byte[]> featureImages, Token token) throws DuplicateRecordException;
	/**
	 * 添加一条验证日志记录,只保存体温数据和现场人脸照片
	 * <br>{@code DEVICE_ONLY}
	 * {@code faceBean}和{@code featureImage}必须全不为{@code null},否则抛出异常
	 * @param logBean 日志记录对象,为{@code null}时只保存陌生人现场照片
	 * @param faceImage 用于保存到数据库的现场采集人脸特征的照片(保存在fl_log.image_md5字段)
	 * @param token 访问令牌
	 * @throws DuplicateRecordException 数据库中存在相同记录
	 */
	@DeriveMethod(methodSuffix="WithFaceImage")
	void addLog(final LogBean logBean, final byte[] faceImage, final Token token) throws DuplicateRecordException;
	
	/**
	 * 添加一组验证日志记录(事务存储),只保存体温数据和现场人脸照片<br>
	 * 所有输入参数的list长度必须一致(不能有{@code null})元素,每2个相同索引位置元素为一组关联的日志记录，参见{@link #addLog(LogBean, byte[], Token)}
	 * @param logBeans 日志记录对象列表,为{@code null}时只保存陌生人现场照片
	 * @param faceImages 用于保存到数据库的现场采集人脸特征的照片
	 * @param token 设备访问令牌
	 * @throws DuplicateRecordException 数据库中存在相同记录
	 */
	@DeriveMethod(methodSuffix="WithFaceImage")
	void addLogs(final List<LogBean> logBeans, final List<byte[]> faceImages, final Token token) throws DuplicateRecordException;

	/**
	 * 日志查询<br>
	 * 根据{@code where}指定的查询条件查询验证日志记录
	 * @param where 'WHERE'开头的SQL条件语句,为{@code null}时返回所有记录
	 * @param startRow 记录起始行号 (first row = 1, last row = -1)
	 * @param numRows 返回记录条数 为负值是返回{@code startRow}开始的所有行
	 * @return 日志记录列表
	 * @deprecated
	 */
	public List<LogBean> loadLogByWhere(String where, int startRow, int numRows);
	/**
	 * 日志查询<br>
	 * 根据{@code where}指定的查询条件查询验证日志记录(只返回访问令牌允许访问的记录)
	 * @param where 'WHERE'开头的SQL条件语句,为{@code null}时返回所有记录
	 * @param startRow 记录起始行号 (first row = 1, last row = -1)
	 * @param numRows 返回记录条数 为负值是返回{@code startRow}开始的所有行
     * @param token 访问令牌
	 * @return 日志记录列表
	 * @since 5.0.0
	 */
	@DeriveMethod(methodSuffix="Safe")
	public List<LogBean> loadLogByWhere(String where, int startRow, int numRows, Token token);
	/**
	 * 日志查询<br>
	 * 根据{@code where}指定的查询条件查询验证日志记录{@link LogLightBean}
	 * @param where 'WHERE'开头的SQL条件语句
	 * @param startRow 记录起始行号 (first row = 1, last row = -1)
	 * @param numRows 返回记录条数 为负值是返回{@code startRow}开始的所有行
	 * @return 日志记录(fl_log_light)列表
	 * @deprecated
	 */
	public List<LogLightBean> loadLogLightByWhere(String where, int startRow, int numRows);
	
	/**
	 * 日志查询<br>
	 * 根据{@code where}指定的查询条件查询验证日志记录{@link LogLightBean}(只返回访问令牌允许访问的记录)
	 * @param where 'WHERE'开头的SQL条件语句
	 * @param startRow 记录起始行号 (first row = 1, last row = -1)
	 * @param numRows 返回记录条数 为负值是返回{@code startRow}开始的所有行
     * @param token 访问令牌
	 * @return 日志记录(fl_log_light)列表
	 * @since 5.0.0
	 */	
	@DeriveMethod(methodSuffix="Safe")
	public List<LogLightBean> loadLogLightByWhere(String where, int startRow, int numRows, Token token);
	/**
	 * 返回符合{@code where}条件的验证日志记录条数
	 * @param where 'WHERE'开头的SQL条件语句,为{@code null}时返回所有记录
	 * @return 返回符合{@code where}条件的记录条数
	 * @deprecated
	 */
	public int countLogLightByWhere(String where);
	
	/**
	 * 返回符合{@code where}条件的验证日志记录条数(只统计访问令牌允许访问的记录)
	 * @param where 'WHERE'开头的SQL条件语句,为{@code null}时返回所有记录
     * @param token 访问令牌
	 * @return 返回符合{@code where}条件的记录条数
	 * @since 5.0.0
	 */	
	@DeriveMethod(methodSuffix="Safe")
	public int countLogLightByWhere(String where, Token token);
	
	/**
	 * 返回满足{@code where}条件的验证日志记录(fl_log)数目
	 * @param where 'WHERE'开头的SQL条件语句,为{@code null}时返回所有记录
	 * @return 返回满足{@code where}条件的日志记录(fl_log)数目
	 * @deprecated
	 */
	public int countLogByWhere(String where);
	/**
	 * 返回满足{@code where}条件的验证日志记录(fl_log)数目(只统计访问令牌允许访问的记录)
	 * @param where 'WHERE'开头的SQL条件语句,为{@code null}时返回所有记录
     * @param token 访问令牌
	 * @return 返回满足{@code where}条件的日志记录(fl_log)数目
	 * @since 5.0.0
	 */
	@DeriveMethod(methodSuffix="Safe")
	public int countLogByWhere(String where, Token token);
	/**
	 * 按天统计指定用户的通行次数<br>
	 * @param personId
	 * @param startDate 统计起始日期,可为{@code null}
	 * @param endDate 统计结束日期,可为{@code null}
	 * @return 返回统计结果，即每个有通行记录的日期(格式:yyyy-MM-dd)的通行次数
	 * @deprecated
	 */
	public Map<String, Integer> countPersonLog(int personId, Long startDate, Long endDate);
	
	/**
	 * 按天统计指定用户的通行次数,超出管理边界则抛出异常<br>
	 * @param personId
	 * @param startDate 统计起始日期,可为{@code null}
	 * @param endDate 统计结束日期,可为{@code null}
	 * @param token 访问令牌
	 * @return 返回统计结果，即每个有通行记录的日期(格式:yyyy-MM-dd)的通行次数
	 * @since 5.0.0
	 */
	@DeriveMethod(methodSuffix="Safe")
	public Map<String, Integer> countPersonLog(int personId, Long startDate, Long endDate, Token token);
	/**
	 * 按天统计指定用户的通行次数<br>
	 * startDate,endDate日期格式为,{@code yyyy-MM-dd}或{@code yyyy-MM-dd HH:mm:ss}或{@code yyyy-MM-dd'T'HH:mm:ss.SSSZ}(ISO8601)格式日期字符串
	 * @param personId
	 * @param startDate 统计起始日期,可为{@code null},日期格式为
	 * @param endDate 统计结束日期,可为{@code null}
	 * @return 返回统计结果，即每个有通行记录的日期(格式:yyyy-MM-dd)的通行次数
	 * @deprecated
	 */
	@DeriveMethod(methodSuffix="TimeStr")
	public Map<String, Integer> countPersonLog(int personId, String startDate, String endDate);

	/**
	 * 按天统计指定用户的通行次数,超出管理边界则抛出异常<br>
	 * startDate,endDate日期格式为,{@code yyyy-MM-dd}或{@code yyyy-MM-dd HH:mm:ss}或{@code yyyy-MM-dd'T'HH:mm:ss.SSSZ}(ISO8601)格式日期字符串
	 * @param personId
	 * @param startDate 统计起始日期,可为{@code null},日期格式为
	 * @param endDate 统计结束日期,可为{@code null}
	 * @param token 访问令牌
	 * @return 返回统计结果，即每个有通行记录的日期(格式:yyyy-MM-dd)的通行次数
	 * @since 5.0.0
	 */
	@DeriveMethod(methodSuffix="TimeStrSafe")
	public Map<String, Integer> countPersonLog(int personId, String startDate, String endDate, Token token);
	/**
	 * 删除{@code where}指定的验证日志(fl_log)
	 * <br>{@code ROOT_ONLY}
	 * @param where 'WHERE'开头的SQL条件语句,为{@code null}时抛出异常
	 * @param token 访问令牌
	 * @return 删除记录的数量
	 */
	public int deleteLogByWhere(String where, Token token);
	/**
     * (主动更新机制实现)返回 fl_log_light.verify_time 字段大于指定时间戳({@code timestamp})的所有记录<br>
	 * @param timestamp 时间戳,{@code yyyy-MM-dd}或{@code yyyy-MM-dd HH:mm:ss}或{@code yyyy-MM-dd'T'HH:mm:ss.SSSZ}(ISO8601)格式日期字符串
	 * @param startRow 记录起始行号 (first row = 1, last row = -1)
	 * @param numRows 返回记录条数 为负值是返回{@code startRow}开始的所有行
	 * @return 日志记录(fl_log_light)列表
	 * @deprecated
     */
	public List<LogLightBean> loadLogLightByVerifyTime(long timestamp,int startRow, int numRows);
	
	/**
     * (主动更新机制实现)返回 fl_log_light.verify_time 字段大于指定时间戳({@code timestamp})的所有记录<br>
     * (只返回访问令牌允许访问的记录)
	 * @param timestamp 时间戳,{@code yyyy-MM-dd}或{@code yyyy-MM-dd HH:mm:ss}或{@code yyyy-MM-dd'T'HH:mm:ss.SSSZ}(ISO8601)格式日期字符串
	 * @param startRow 记录起始行号 (first row = 1, last row = -1)
	 * @param numRows 返回记录条数 为负值是返回{@code startRow}开始的所有行
     * @param token 访问令牌
	 * @return 日志记录(fl_log_light)列表
	 * @since 5.0.0
     */
	@DeriveMethod(methodSuffix="Safe")
	public List<LogLightBean> loadLogLightByVerifyTime(long timestamp, int startRow, int numRows, Token token);
	/**
	 * (主动更新机制实现)返回 fl_log_light.verify_time 字段大于指定时间戳({@code timestamp})的所有记录<br>
	 * @param timestamp 时间戳
	 * @param startRow 记录起始行号 (first row = 1, last row = -1)
	 * @param numRows 返回记录条数 为负值是返回{@code startRow}开始的所有行
	 * @return 日志记录(fl_log_light)列表
	 * @deprecated
	 */
	@DeriveMethod(methodSuffix="Timestr")
	public List<LogLightBean> loadLogLightByVerifyTime(String timestamp, int startRow, int numRows);

	/**
     * (主动更新机制实现)返回 fl_log_light.verify_time 字段大于指定时间戳({@code timestamp})的所有记录<br>
     * (只返回访问令牌允许访问的记录)
	 * @param timestamp 时间戳,{@code yyyy-MM-dd}或{@code yyyy-MM-dd HH:mm:ss}或{@code yyyy-MM-dd'T'HH:mm:ss.SSSZ}(ISO8601)格式日期字符串
	 * @param startRow 记录起始行号 (first row = 1, last row = -1)
	 * @param numRows 返回记录条数 为负值是返回{@code startRow}开始的所有行
     * @param token 访问令牌
	 * @return 日志记录(fl_log_light)列表
	 * @since 5.0.0
     */
	@DeriveMethod(methodSuffix="TimestrSafe")
	public List<LogLightBean> loadLogLightByVerifyTime(String timestamp, int startRow, int numRows, Token token);
    /**
     * 返回fl_log_light.verify_time 字段大于指定时间戳({@code timestamp})的记录总数
     * @param timestamp 时间戳
     * @return 满足条件的记录条数
     * @see #countLogLightByWhere(String)
     * @deprecated
     */
	public int countLogLightByVerifyTime(long timestamp);

	/**
     * 返回fl_log_light.verify_time 字段大于指定时间戳({@code timestamp})的记录总数(只统计访问令牌允许访问的记录)
     * @param timestamp 时间戳
     * @param token 访问令牌
     * @return 满足条件的记录条数
     * @see #countLogLightByWhere(String)
     * @since 5.0.0
     */
	@DeriveMethod(methodSuffix="Safe")
	public int countLogLightByVerifyTime(long timestamp, Token token);
    /**
     * 返回fl_log_light.verify_time 字段大于指定时间戳({@code timestamp})的记录总数
     * @param timestamp 时间戳,{@code yyyy-MM-dd}或{@code yyyy-MM-dd HH:mm:ss}或{@code yyyy-MM-dd'T'HH:mm:ss.SSSZ}(ISO8601)格式日期字符串
     * @return 满足条件的记录条数
     * @see #countLogLightByWhere(String)
     * @deprecated
     */
	@DeriveMethod(methodSuffix="Timestr")
	public int countLogLightByVerifyTime(String timestamp);
    /**
     * 返回fl_log_light.verify_time 字段大于指定时间戳({@code timestamp})的记录总数(只统计访问令牌允许访问的记录)
     * @param timestamp 时间戳,{@code yyyy-MM-dd}或{@code yyyy-MM-dd HH:mm:ss}或{@code yyyy-MM-dd'T'HH:mm:ss.SSSZ}(ISO8601)格式日期字符串
     * @param token 访问令牌
     * @return 满足条件的记录条数
     * @see #countLogLightByWhere(String)
     * @since 5.0.0
     */
	@DeriveMethod(methodSuffix="TimestrSafe")
	public int countLogLightByVerifyTime(String timestamp, Token token);
	/**
	 * 添加一条错误日志<br>
	 * ErrorLogBean.catalog字段为错误类型分类,
	 * 以'/'分隔的多级类型定义,应用程序可自定义,
	 * 顶级分类:service(服务异常),device(设备异常),application(应用异常),
	 * 例如: device/network
	 * @param errorLogBean 错误日志记录对象
	 * @param token 访问令牌
	 */
	public void addErrorLog(ErrorLogBean errorLogBean, Token token);
	/**
	 * 错误日志查询<br>
	 * 根据{@code where}指定的查询条件查询错误日志记录
	 * @param where 'WHERE'开头的SQL条件语句,为{@code null}时返回所有记录
	 * @param startRow 记录起始行号 (first row = 1, last row = -1)
	 * @param numRows 返回记录条数 为负值是返回{@code startRow}开始的所有行
	 * @return 错误日志记录列表
	 */
	public List<ErrorLogBean> loadErrorLogByWhere(String where, int startRow, int numRows);
	/**
	 * 返回满足{@code where}条件的错误日志记录(fl_error_log)数目
	 * @param where 'WHERE'开头的SQL条件语句,为{@code null}时返回所有记录
	 * @return 返回满足{@code where}条件的错误日志记录(fl_error_log)数目
	 */
	public int countErrorLogByWhere(String where);
	/**
	 * 删除{@code where}指定的错误日志(fl_error_log)
	 * <br>{@code ROOT_ONLY}
	 * @param where 'WHERE'开头的SQL条件语句,为{@code null}时抛出异常
	 * @param token 访问令牌
	 * @return 删除记录的数量
	 */
	public int deleteErrorLogByWhere(String where, Token token);

	/**
	 * 判断{@code md5}指定的图像记录是否存在
	 * @param md5 图像的MD5校验码
	 * @return 记录存在返回{@code true},否则返回{@code false}
	 */
	public boolean existsImage(String md5);

	/**
	 * 保存图像数据,如果图像数据已经存在，则抛出异常
	 * @param imageData 图像数据
	 * @param deviceId 图像来源设备id,可为null
	 * @param faceBean 关联的人脸信息对象,可为null
	 * @param personId 关联的人员id(fl_person.id),可为null
	 * @param token 访问令牌
	 * @return 保存的图像记录
	 * @throws DuplicateRecordException 数据库中已经存在要保存的图像数据
	 */
	public ImageBean addImage(byte[] imageData, @CodegenInvalidValue("-1") Integer deviceId, FaceBean faceBean, @CodegenInvalidValue("-1")Integer personId, Token token)
			throws DuplicateRecordException;

	/**
	 * 判断md5指定的特征记录是否存在
	 * @param md5
	 * @return true if md5exist 
	 */
	public boolean existsFeature(String md5);

	/**
	 * 增加一个人脸特征记录，如果记录已经存在则抛出异常<br>
	 * @param feature 人脸特征数据
	 * @param featureVersion 特征(SDk)版本号
	 * @param personId 关联的人员id(fl_person.id),可为null
	 * @param faecBeans 生成特征数据的人脸信息对象(可以是多个人脸对象合成一个特征),可为null
	 * @param removed 已经存在的特征记录ID(MD5),可为{@code null},不为{@code null}时会先删除指定的特征,记录不存在则抛出异常
	 * @param token 访问令牌
	 * @return 保存的人脸特征记录{@link FeatureBean}
	 * @throws DuplicateRecordException 
	 */
	public FeatureBean addFeature(byte[] feature, String featureVersion,@CodegenInvalidValue("-1") Integer personId, List<FaceBean> faecBeans, String removed, Token token) throws DuplicateRecordException;
	
	/**
	 * 增加一个人脸特征记录，如果记录已经存在则抛出异常<br>
	 * 适用于一张人脸图像提取一个人脸特征的算法<br>
	 * @param feature 特征数据
	 * @param featureVersion 特征(SDk)版本号
	 * @param personId 关联的人员id(fl_person.id),可为null
	 * @param asIdPhotoIfAbsent 如果{@code personId}指定的记录没指定身份照片,
	 * 是否用{@code featurePhoto}作为身份照片,{@code featurePhoto}为{@code null}时无效
	 * @param featurePhoto 生成人脸特征的原始照片,如果不要求保留原始照片可为null
	 * @param faceBean 生成特征数据的人脸信息对象(可以是多个人脸对象合成一个特征),可为null
	 * @param removed 已经存在的特征记录ID(MD5),可为{@code null},不为{@code null}时会先删除指定的特征,记录不存在则抛出异常
	 * @param token (设备)访问令牌
	 * @return 保存的人脸特征记录{@link FeatureBean}
	 * @throws DuplicateRecordException
	 * @since 2.1.2
	 */
	@DeriveMethod(methodSuffix="WithImage")
	FeatureBean addFeature(final byte[] feature, String featureVersion, @CodegenInvalidValue("-1") final Integer personId, final boolean asIdPhotoIfAbsent, final byte[] featurePhoto, final FaceBean faceBean, String removed, Token token)throws DuplicateRecordException;

	/**
	 * 增加一个人脸特征记录,特征数据由faceInfo指定的多张图像合成，如果记录已经存在则抛出异常<br>
	 * {@code photos}与{@code faces}为提取特征{@code feature}的人脸照片对应的人脸位置对象，必须一一对应
	 * @param feature 特征数据
	 * @param featureVersion 特征(SDk)版本号
	 * @param personId 关联的人员id(fl_person.id),可为null
	 * @param photos 检测到人脸的照片列表
	 * @param faces 检测人脸信息列表
	 * @param removed 已经存在的特征记录ID(MD5),可为{@code null},不为{@code null}时会先删除指定的特征,记录不存在则抛出异常
	 * @param token (设备)访问令牌
	 * @return 保存的人脸特征记录{@link FeatureBean}
	 * @throws DuplicateRecordException 
	 */
	@DeriveMethod(methodSuffix="Multi")
	public FeatureBean addFeature(byte[] feature, String featureVersion, @CodegenInvalidValue("-1") Integer personId, List<byte[]> photos, List<FaceBean> faces, String removed, Token token)
			throws DuplicateRecordException;

	/**
	 * 删除featureMd5指定的特征记录及关联的face记录
	 * @param featureMd5
	 * @param deleteImage 为{@code true}则删除关联的 image记录(如果该图像还关联其他特征则不删除)
	 * @param token 访问令牌
	 * @return 返回删除的特征记录关联的图像(image)记录的MD5<br>
	 */
	public List<String> deleteFeature(String featureMd5, boolean deleteImage, Token token);

	/**
	 * 删除 personId 关联的所有特征(feature)记录
	 * @param personId
	 * @param deleteImage 是否删除关联的 image记录
	 * @param token 访问令牌
	 * @return 删除记录的数量
	 * @see #deleteFeature(String, boolean, Token)
	 */
	public int deleteAllFeaturesByPersonId(int personId, boolean deleteImage, Token token);
	/**
	 * 根据MD5校验码返回人脸特征数据记录
	 * @param md5
	 * @return 如果数据库中没有对应的数据则返回null
	 */
	public FeatureBean getFeature(String md5);

	/**
	 * 根据MD5校验码返回人脸特征数据记录
	 * @param md5List md5列表
	 * @return {@link FeatureBean}列表
	 */
	public List<FeatureBean> getFeatures(List<String> md5List);
	/**
	 * 返回指定人员{@code personId}关联的所有特征<br>
	 * @param personId
	 * @return 返回 fl_feature.md5  列表
	 */
	public List<String> getFeaturesOfPerson(int personId);

	/**
	 * 返回 persionId 关联的指定SDK的人脸特征记录
	 * @param personId 人员id(fl_person.id)
	 * @param sdkVersion 算法(SDK)版本号
	 * @return 返回 fl_feature.md5  列表
	 */
	public List<String> getFeaturesByPersonIdAndSdkVersion(int personId, String sdkVersion);

	/**
	 * 返回在指定设备上允许通行的所有特征记录<br>
	 * 此方法主要设计用于不能通过长连接侦听redis频道的设备(如人脸锁)。
	 * @param deviceId 设备ID
	 * @param ignoreSchedule 是否忽略时间过滤器(fl_permit.schedule字段)的限制
	 * @param sdkVersion 特征版本号
	 * @param excludeFeatureIds 要排除的特征记录id(MD5) ,可为{@code null}
	 * @param timestamp 不为{@code null}时返回大于指定时间戳的所有fl_feature记录
	 * @return 返回 fl_feature.md5  列表
	 */
	public List<String> getFeaturesPermittedOnDevice(int deviceId, boolean ignoreSchedule, String sdkVersion, List<String> excludeFeatureIds, Long timestamp);
	
	/**
	 * 获取指定人脸特征关联的人脸特征记录
	 * @param imageMd5 图像数据的MD校验码,为空或{@code null}或记录不存在返回空表
	 * @return 特征记录id列表
	 */
	public List<String> getFeaturesOfImage(String imageMd5);
	
	/**
	 * 根据MD5校验码返回人脸特征数据
	 * @param md5
	 * @return 二进制数据字节数组,如果数据库中没有对应的数据则返回null
	 */
	public byte[] getFeatureBytes(String md5);

	/**
	 * 根据MD5校验码返回人脸特征数据<br>
	 * 此方法与{@link #getFeatureBytes(String)}的不同在于多了一个boolean型的{@code truncation}参数指定是否只返回MD5计算范围内数据。
	 * 对于大部分算法，此方法返回值与{@link #getFeatureBytes(String)}没有什么不同，但是对于定义了MD5计算范围(固定不变的数据)的算法。
	 * {@code truncation}为{@code true}时此方法只会将特征中MD5计算范围(offset,length)的数据返回，
	 * 这样做的主要目的是为了当特征数据中存在无效数据时减少特征数据传送量
	 * @param md5
	 * @param truncation 是否只提取MD5计算范围内的特征数据
	 * @return 二进制数据字节数组,如果数据库中没有对应的数据则返回null
	 */
	@DeriveMethod(methodSuffix="Truncation")
	public byte[] getFeatureBytes(String md5, boolean truncation);
	/**
	 * 根据MD5校验码返回人脸特征数据<br>
	 * 此方法与{@link #getFeatureBytes(String, boolean)}的不同在于可以返回多个MD5的特征。
	 * @param md5List
	 * @param truncation 是否只提取MD5计算范围内的特征数据
	 * @return 二进制数据字节数组列表,列表长度与输入列表长度一致,如果数据库中没有对应的数据则返回长度为0的空数组
	 */
	public List<byte[]> getFeatureBytesList(List<String> md5List, boolean truncation);	

	/**
	 * 根据图像的MD5校验码返回图像数据
	 * @param imageMD5
	 * @return 二进制数据字节数组,如果数据库中没有对应的数据则返回null
	 */
	public byte[] getImageBytes(String imageMD5);

	/**
	 * 根据提供的主键ID,返回图像数据<br>
	 * example:<br>
	 * 获取指定日志表的现场采集照片<br>
	 * <pre>getImageBytes(log_id,'LOG')</pre>
	 * 获取人员标准照<br>
	 * <pre>getImageBytes(person_id,'PERSON')</pre>
	 * @param primaryKey 数据库表的主键值,根据 refType的类型不同，primaryKey代表不同表的主键(类型为整数的主键需要转为十进制字符串)
	 * @param refType 指定 primaryKey 的引用类型,如下:
	 * 					<ul>
	 * 						<li>DEFAULT 返回 fl_image表指定的图像数据</li>
	 * 						<li>IMAGE 返回 fl_image表指定的图像数据</li>
	 * 						<li>PERSON 返回 fl_person表中的image_md5字段指定的图像数据</li>
	 * 						<li>FACE 返回 fl_face表中的image_md5字段指定的图像数据</li>
	 * 						<li>LOG 返回 fl_log表中的compare_face字段间接指定的图像数据</li>
	 * 						<li>LIGHT_LOG 返回 fl_log_light视图对应fl_log表记录中的compare_face字段的图像数据</li>
	 * 					</ul>
	 * 				<br>为{@code null}则默认为DEFAULT
	 * @return primaryKey 为 null时或记录不存在返回null
	 */
	@DeriveMethod(methodSuffix="Ref")
	public byte[] getImageBytes(String primaryKey, String refType);

	/**
	 * 根据图像的MD5校验码返回图像记录
	 * @param imageMD5
	 * @return {@link ImageBean} ,如果没有对应记录则返回null
	 */	
	public ImageBean getImage(String imageMD5);
	/**
	 * 根据数据库表的主键值返回图像记录
	 * @param primaryKey 数据库表的主键值,根据 refType的类型不同，primaryKey代表不同表的主键(类型为整数的主键需要转为十进制字符串)
	 * @param refType 指定 primaryKey 的引用类型,如下:
	 * 					<ul>
	 * 						<li>DEFAULT 返回 fl_image表指定的图像数据</li>
	 * 						<li>IMAGE 返回 fl_image表指定的图像数据</li>
	 * 						<li>PERSON 返回 fl_person表中的image_md5字段指定的图像数据</li>
	 * 						<li>FACE 返回 fl_face表中的image_md5字段指定的图像数据</li>
	 *						<li>FEATURE 返回 fl_feature 人脸特征记录关联的图像数据(有多个图像记录时返回第一条)</li>
	 * 						<li>LOG 返回 fl_log表中的compare_face字段间接指定的图像数据</li>
	 * 						<li>LIGHT_LOG 返回 fl_log_light视图对应fl_log表记录中的compare_face字段的图像数据</li>
	 * 					</ul>
	 * 				<br>为{@code null}则默认为DEFAULT
	 * @return {@link ImageBean} ,如果没有对应记录则返回null
	 */	
	@DeriveMethod(methodSuffix="Ref")
	public ImageBean getImage(String primaryKey, String refType);
	
	/**
	 * 返回featureMd5的人脸特征记录关联的所有图像记录id(MD5) 
	 * @param featureMd5 人脸特征id(MD5)
	 * @return 返回 fl_image.md5  列表,没找到返回空表
	 */
	public List<String> getImagesAssociatedByFeature(String featureMd5);
	
	/**
	 * 返回faceId指定的人脸信息记录
	 * @param faceId
	 * @return {@link FaceBean} ,如果没有对应记录则返回null
	 */
	public FaceBean getFace(int faceId);
	
	/**
	 * 获取指定人脸特征关联的人脸记录
	 * @param featureMd5 人脸特征记录id(MD校验码),为空或{@code null}或记录不存在返回空表
	 * @return {@link FaceBean}列表
	 */
	public List<FaceBean> getFacesOfFeature(String featureMd5);
	
	/**
	 * 获取指定图像关联的人脸记录
	 * @param imageMd5 图像数据的MD校验码,为空或{@code null}或记录不存在返回空表
	 * @return {@link FaceBean}列表
	 */
	public List<FaceBean> getFacesOfImage(String imageMd5);

	/**
	 * 返回featureMd5的人脸特征记录关联的设备id<br>
	 * @param featureMd5
	 * @return 如果没有关联的设备则返回{@code null}
	 */
	public Integer getDeviceIdOfFeature(String featureMd5);
	/**
	 * 删除imageMd5指定图像及其缩略图
	 * @param imageMd5
	 * @param token 访问令牌
	 * @return 删除成功返回1,否则返回0
	 */
	public int deleteImage(String imageMd5, Token token);

	/**
	 * 判断id指定的设备记录是否存在
	 * @param id
	 * @return true if device exists
	 */
	public boolean existsDevice(int id);
	/**
	 * 保存设备记录
	 * <br>{@code PERSON_ONLY}
	 * @param deviceBean
	 * @param token 访问令牌
	 * @return saved DeviceBean 
	 */
	public DeviceBean saveDevice(DeviceBean deviceBean, Token token);
	/**
	 * 更新设备记录(必须是已经存在的设备记录，否则抛出异常)
	 * @param deviceBean
	 * @param token 访问令牌
	 * @return 返回设备记录
	 */
	public DeviceBean updateDevice(DeviceBean deviceBean, Token token);
	/**
	 * 返回{@code deviceId}指定的设备记录
	 * @param deviceId
	 * @return 返回设备记录
	 */
	public DeviceBean getDevice(int deviceId);
	
	/**
	 * 根据设备MAC地址查找指定的设备记录
	 * @param mac
	 * @return 返回设备记录
	 */
	public DeviceBean getDeviceByMac(String mac);
	/**
	 * 返回 {@code idList} 指定的设备记录
	 * @param idList
	 * @return 返回设备记录列表
	 */
	public List<DeviceBean> getDevices(List<Integer> idList);
	/**
	 * 根据{@code where}指定的查询条件查询设备记录
	 * @param where 'WHERE'开头的SQL条件语句,为{@code null}时返回所有记录
	 * @param startRow 记录起始行号 (first row = 1, last row = -1)
	 * @param numRows 返回记录条数 为负值是返回{@code startRow}开始的所有行
	 * @return 返回设备记录列表
	 * @deprecated
	 */
	public List<DeviceBean> loadDeviceByWhere(String where,int startRow, int numRows);
	/**
	 * 根据{@code where}指定的查询条件查询设备记录(只返回访问令牌允许访问的记录)
	 * @param where 'WHERE'开头的SQL条件语句,为{@code null}时返回所有记录
	 * @param startRow 记录起始行号 (first row = 1, last row = -1)
	 * @param numRows 返回记录条数 为负值是返回{@code startRow}开始的所有行
	 * @param token 访问令牌
	 * @return 返回设备记录列表
     * @since 5.0.0
	 */
	@DeriveMethod(methodSuffix="Safe")
	public List<DeviceBean> loadDeviceByWhere(String where, int startRow, int numRows, Token token);
	/**
	 * 返回满足{@code where} SQL条件语句的fl_device记录总数
	 * @param where 'WHERE'开头的SQL条件语句,为{@code null}时返回所有记录
	 * @return 返回设备ID列表
	 * @deprecated
	 */
	public int countDeviceByWhere(String where);
	/**
	 * 返回满足{@code where} SQL条件语句的fl_device记录总数(只统计访问令牌允许访问的记录)
	 * @param where 'WHERE'开头的SQL条件语句,为{@code null}时返回所有记录
	 * @param token 访问令牌
	 * @return 返回设备ID列表
     * @since 5.0.0
	 */
	@DeriveMethod(methodSuffix="Safe")
	public int countDeviceByWhere(String where, Token token);
	/**
	 * 根据{@code where}指定的查询条件查询设备记录
	 * @param where 'WHERE'开头的SQL条件语句,为{@code null}时返回所有记录
	 * @return 返回设备ID列表
	 * @deprecated
	 */
	public List<Integer> loadDeviceIdByWhere(String where);
	/**
	 * 根据{@code where}指定的查询条件查询设备记录(只返回访问令牌允许访问的记录)
	 * @param where 'WHERE'开头的SQL条件语句,为{@code null}时返回所有记录
	 * @param token 访问令牌
	 * @return 返回设备ID列表
     * @since 5.0.0
	 */
	@DeriveMethod(methodSuffix="Safe")
	public List<Integer> loadDeviceIdByWhere(String where, Token token);
	////////////////////////////////DeviceGroupBean/////////////
	/**
	 * 保存设备组记录
	 * <br>{@code PERSON_ONLY}
	 * @param deviceGroupBean
	 * @param token 访问令牌
	 * @return DeviceGroupBean saved
	 */
	public DeviceGroupBean saveDeviceGroup(DeviceGroupBean deviceGroupBean, Token token);
	/**
	 * 根据设备组id返回数据库记录
	 * @param deviceGroupId
	 * @return DeviceGroupBean saved
	 */
	public DeviceGroupBean getDeviceGroup(int deviceGroupId);
	/**
	 * 返回设备组id列表指定的数据库记录
	 * @param groupIdList
	 * @return 设备组记录列表
	 */
	public List<DeviceGroupBean> getDeviceGroups(List<Integer> groupIdList);
	/**
	 * 删除{@code deviceGroupId}指定的设备组<br>
	 * 组删除后，所有子节点记录不会被删除，但parent字段会被自动默认为{@code null}
	 * <br>{@code PERSON_ONLY}
	 * @param deviceGroupId
	 * @param token 访问令牌
	 * @return  返回删除的记录条数
	 */
	public int deleteDeviceGroup(int deviceGroupId, Token token);

	/**
	 *  删除设备id指定的设备记录
	 * <br>{@code PERSON_ONLY}
	 * @param id
	 * @param token 人员令牌
	 * @return 返回删除的记录条数(1),如果记录不存在返回0
	 */
	public boolean deleteDevice(int id, Token token);

	/**
	 * 删除设备MAC地址指定的设备记录
	 * <br>{@code PERSON_ONLY}
	 * @param mac 设备MAC地址(12位HEX字符串)
	 * @param token 人员令牌
	 * @return 返回删除的记录条数(1),如果记录不存在返回0
	 */
	public boolean deleteDeviceByMac(String mac, Token token);

	/**
	 * 返回{@code deviceGroupId}指定的设备组下的所有子节点(设备组)<br>
	 * 如果没有子节点则返回空表
	 * @param deviceGroupId
	 * @return 设备组ID列表
	 */
	public List<Integer> getSubDeviceGroup(int deviceGroupId);
	/**
	 * 返回{@code deviceGroupId}指定的设备组下属的所有设备记录<br>
	 * 如果没有下属设备记录则返回空表
	 * @param deviceGroupId
	 * @return 设备id列表
	 */
	public List<Integer> getDevicesOfGroup(int deviceGroupId);
    /**
     * 返回({@code deviceGroupId})指定的fl_device_group记录的所有的父节点(包括自己)<br>
     * 自引用字段:fl_device_group(parent)
	 * @param deviceGroupId
	 * @return  如果{@code deviceGroupId}无效则返回空表
     */
	public List<Integer> listOfParentForDeviceGroup(int deviceGroupId);
	
	/**
     * 递归返回(deviceGroupId))指定的fl_device_group记录的所有的子节点(包括自己)<br>
     * 自引用字段:fl_device_group(parent)
	 * @param deviceGroupId
	 * @return 如果{@code deviceGroupId}无效则返回空表
	 * @since 2.1.2
	 */
	public List<Integer> childListForDeviceGroup(int deviceGroupId);

	/**
     * 返回({@code deviceId})指定的设备所属所有设备组<br>
	 * @param deviceId
	 * @return 如果{@code deviceId}无效则返回空表
	 * @see #listOfParentForDeviceGroup(int)
	 */
	public List<Integer> getDeviceGroupsBelongs(int deviceId);
	////////////////////////////////PersonGroupBean/////////////
	/**
	 * 保存人员组记录
	 * <br>{@code PERSON_ONLY}
	 * @param personGroupBean
	 * @param token 访问令牌
	 * @return 保存的人员组记录
	 */
	public PersonGroupBean savePersonGroup(PersonGroupBean personGroupBean, Token token);
	/**
	 * 根据人员组id返回数据库记录
	 * @param personGroupId
	 * @return 人员组记录
	 */
	public PersonGroupBean getPersonGroup(int personGroupId);
	/**
	 * 返回人员组id列表指定的数据库记录
	 * @param groupIdList
	 * @return 人员组记录列表
	 */
	public List<PersonGroupBean> getPersonGroups(List<Integer> groupIdList);
	/**
	 * 删除{@code personGroupId}指定的人员组<br>
	 * 组删除后，所有子节点记录不会被删除，但parent字段会被自动默认为{@code null}
	 * <br>{@code PERSON_ONLY}
	 * @param personGroupId
	 * @param token 访问令牌
	 * @return 删除的记录数量
	 */
	public int deletePersonGroup(int personGroupId, Token token);
	/**
	 * 返回{@code personGroupId}指定的人员组下的所有子节点(人员组)<br>
	 * 如果没有子节点则返回空表
	 * @param personGroupId
	 * @return 人员组ID列表
	 */
	public List<Integer> getSubPersonGroup(int personGroupId);
	/**
	 * 返回{@code deviceGroupId}指定的人员组下属的所有人员记录<br>
	 * 如果没有下属人员记录则返回空表
	 * @param personGroupId
	 * @return 人员ID列表
	 */
	public List<Integer> getPersonsOfGroup(int personGroupId);
    /**
     * 返回({@code personGroupId})指定的fl_person_group记录的所有的父节点(包括自己)<br>
     * 自引用字段:fl_person_group(parent)
	 * @param personGroupId
	 * @return  如果{@code personGroupId}无效则返回空表
     */
	public List<Integer> listOfParentForPersonGroup(int personGroupId);
	
	/**
     * 递归返回(personGroupId))指定的fl_person_group记录的所有的子节点(包括自己)<br>
     * 自引用字段:fl_person_group(parent)
	 * @param personGroupId
	 * @return 如果{@code personGroupId}无效则返回空表
	 * @since 2.1.2
	 */
	public List<Integer> childListForPersonGroup(int personGroupId);
	/**
     * 返回({@code personId})指定的人员所属所有人员组<br>
	 * @param personId
	 * @return 如果{@code personId}无效则返回空表
	 * @see #listOfParentForPersonGroup(int)
	 */
	public List<Integer> getPersonGroupsBelongs(int personId);
    /**
     * 查询{@code where} SQL条件语句指定的记录
     * @param where 'WHERE'开头的SQL条件语句,为{@code null}时返回所有记录
     * @param startRow 返回记录的起始行(首行=1,尾行=-1)
     * @param numRows 返回记录条数(小于0时返回所有记录)
     * @return 设备组ID列表
     * @deprecated replaced by {@link #loadDeviceGroupIdByWhere(String)}
     */
    public List<Integer> loadDeviceGroupByWhere(String where,int startRow, int numRows);
    
    /**
     * 返回满足{@code where} SQL条件语句的fl_device_group记录总数
     * @param where 'WHERE'开头的SQL条件语句,为{@code null}时返回所有记录
     * @return 返回满足{@code where} SQL条件语句的fl_device_group记录总数
     * @deprecated
     */
    public int countDeviceGroupByWhere(String where);
    
    /**
     * 返回满足{@code where} SQL条件语句的fl_device_group记录总数(只统计访问令牌允许访问的记录)
     * @param where 'WHERE'开头的SQL条件语句,为{@code null}时返回所有记录
     * @param token 访问令牌
     * @return 返回满足{@code where} SQL条件语句的fl_device_group记录总数
     * @since 5.0.0
     */
    @DeriveMethod(methodSuffix="Safe")
    public int countDeviceGroupByWhere(String where, Token token);
    /** 
     * 查询{@code where}条件指定的记录
     * @param where 'WHERE'开头的SQL条件语句,为{@code null}时返回所有记录
     * @return 返回查询结果记录的主键
     * @deprecated
     */
    public List<Integer> loadDeviceGroupIdByWhere(String where);

    /** 
     * 查询{@code where}条件指定的记录(只返回访问令牌允许访问的记录)
     * @param where 'WHERE'开头的SQL条件语句,为{@code null}时返回所有记录
     * @param token 访问令牌
     * @return 返回查询结果记录的主键
     * @since 5.0.0
     */
    @DeriveMethod(methodSuffix="Safe")
    public List<Integer> loadDeviceGroupIdByWhere(String where, Token token);
	/////////////////////MANAGEMENT BORDER/////
    /**
	 * 创建管理边界<br>
	 * 设置fl_person_group.root_group和fl_device_group.root_group字段互相指向<br>
	 * 没有找到personGroupId或deviceGroupId指定的记录抛出异常,
	 * 以事务操作方式更新数据库<br>
	 * <br>{@code ROOT_ONLY}<br>
	 * @param personGroupId 人员组id
     * @param deviceGroupId 设备组id
     * @param token 访问令牌
	 */
	public void bindBorder(int personGroupId, int deviceGroupId, Token token);
	/**
	 * 删除管理边界<br>
	 * 删除fl_person_group.root_group和fl_device_group.root_group字段的互相指向,设置为{@code null},
	 * 以事务操作方式更新数据库<br>
	 * 如果personGroupId和deviceGroupId不存在绑定关系则跳过,
	 * 没有找到personGroupId或deviceGroupId指定的记录抛出异常<br>
	 * <br>{@code ROOT_ONLY}<br>
	 * @param personGroupId 人员组id
	 * @param deviceGroupId 设备组id
	 * @param token 访问令牌
	 */
	public void unbindBorder(int personGroupId, int deviceGroupId, Token token);
    /**
	 * 返回personId所属的管理边界人员组id<br>
	 * 在personId所属组的所有父节点中自顶向下查找第一个{@code fl_person_group.root_group}字段不为空的人员组，返回此记录组id<br>
	 * 没有找到personId指定的记录抛出异常
	 * @param personId 人员id
	 * @return {@code fl_person_group.root_group}字段不为空的记录id,没有找到则返回{@code null}
	 */
	public Integer rootGroupOfPerson(int personId);
	/**
	 * 返回groupId指定人员组的管理边界人员组id<br>
	 * 在groupId指定人员组的所有父节点中自顶向下查找第一个{@code fl_person_group.root_group}字段不为空的人员组，返回此记录组id
	 * @param groupId 人员组id
	 * @return {@code fl_person_group.root_group}字段不为空的记录id,没有找到则返回{@code null}
	 */
	public Integer rootGroupOfPersonGroup(int groupId);
	/**
	 * 返回deviceId所属的管理边界设备组id<br>
	 * 在deviceId所属组的所有父节点中自顶向下查找第一个{@code fl_device_group.root_group}字段不为空的组，返回此记录id<br>
	 * 没有找到deviceId指定的记录抛出异常
	 * @param deviceId 设备ID
	 * @return {@code fl_device_group.root_group}字段不为空的记录id,没有找到则返回{@code null}
	 */
	public Integer rootGroupOfDevice(int deviceId);
	/**
	 * 返回groupId指定设备组的管理边界设备组id<br>
	 * 在groupId指定设备组的所有父节点中自顶向下查找第一个{@code fl_device_group.root_group}字段不为空的组，返回此记录id
	 * @param groupId 设备组id
	 * @return {@code fl_device_group.root_group}字段不为空的记录id,没有找到则返回{@code null}
	 */
	public Integer rootGroupOfDeviceGroup(int groupId);

	/////////////////////PERMIT/////

    /**
     * 保存通行权限(permit)记录
     * <br>{@code PERSON_ONLY}
	 * @param permitBean 要修改或增加的fl_permit记录
	 * @param token 访问令牌
	 * @return 保存的{@link PermitBean}
	 */
	public PermitBean savePermit(PermitBean permitBean, Token token);
	/**
	 * 修改指定记录的的字段值(String类型)<br>
	 * 如果记录不存在则创建deviceGroupId和personGroupId之间的MANY TO MANY 联接表(fl_permit)记录,
	 * <br>{@code PERSON_ONLY}
     * @param deviceGroupId 设备组id
	 * @param personGroupId 人员组id
	 * @param column 字段名，允许的字段名为'schedule','pass_limit'
	 * @param value 字段值
	 * @param token 访问令牌
     * @return (fl_permit)记录
     */
	@DeriveMethod(methodSuffix="WithColumn")
	public PermitBean savePermit(int deviceGroupId,int personGroupId, String column, String value, Token token);
	/**
	 * 删除fl_device_group和fl_person_group之间的MANY TO MANY 联接表(fl_permit)记录<br>
	 * @param deviceGroupId 设备组id
	 * @param personGroupId 人员组id
	 * @param token
	 * @return 删除成功返回1,否则返回0
	 * @since 2.1.2
	 */
	@DeriveMethod(methodSuffix="ById")
	int deletePermit(int deviceGroupId, int personGroupId, Token token);

	/**
	 * 从permit表删除指定{@code personGroupId}指定人员组的在所有设备上的通行权限
	 * @param personGroupId 
	 * @param token 令牌
	 * @return 删除的记录条数
	 */
	int deletePersonGroupPermit(int personGroupId, Token token);

	/**
	 * 从permit表删除指定{@code deviceGroupId}指定设备组上的人员通行权限
	 * @param deviceGroupId 
	 * @param token 令牌
	 * @return 删除的记录条数
	 */
	int deleteGroupPermitOnDeviceGroup(int deviceGroupId, Token token);
	/**
	 * 获取人员组通行权限<br>
	 * 返回{@code personGroupId}指定的人员组在{@code deviceGroupId}指定的设备组上是否允许通行,
	 * 本方法会对{@code personGroupId}的父结点向上回溯：
	 * {@code personGroupId } 及其父结点,任何一个在permit表存在与{@code deviceId}所属设备级的关联记录中就返回true，
	 * 输入参数为{@code null}或找不到指定的记录则返回false
	 * @param deviceGroupId
	 * @param personGroupId
	 * @return 允许通行返回指定的{@link PermitBean}记录，否则返回{@code null}
	 */
	PermitBean getGroupPermitOnDeviceGroup(int deviceGroupId, int personGroupId);
	/**
	 * 获取人员组通行权限<br>
	 * 返回{@code personGroupId}指定的人员组在{@code deviceId}设备上是否允许通行,
	 * 本方法会对{@code personGroupId}的父结点向上回溯：
	 * {@code personGroupId } 及其父结点,任何一个在permit表存在与{@code deviceId}所属设备级的关联记录中就返回true，
	 * 输入参数为{@code null}或找不到指定的记录则返回false
	 * @param deviceId
	 * @param personGroupId
	 * @return 允许通行返回指定的{@link PermitBean}记录，否则返回{@code null}
	 */
	public PermitBean getGroupPermit(int deviceId,int personGroupId);
	/**
	 * 获取人员通行权限<br>
	 * 返回{@code personId}指定的人员在{@code deviceId}设备上是否允许通行
	 * @param deviceId
	 * @param personId
	 * @return 允许通行返回指定的{@link PermitBean}记录，否则返回{@code null}
	 */
	public PermitBean getPersonPermit(int deviceId,int personId);
	/**
	 * 参见 {@link #getGroupPermit(int, int)}
	 * @param deviceId
	 * @param personGroupIdList
	 * @return 通行权限记录列表
	 */
	public List<PermitBean> getGroupPermits(int deviceId,List<Integer> personGroupIdList);
	/**
	 * 参见 {@link #getPersonPermit(int, int) }
	 * @param deviceId
	 * @param personIdList
	 * @return 通行权限记录列表
	 */
	public List<PermitBean> getPersonPermits(int deviceId,List<Integer> personIdList);

	/**
	 * 从permit表返回允许在{@code deviceGroupId}指定的设备组通过的所有人员组{@link PersonGroupBean}对象的id<br>
	 *  不排序,不包含重复id,本方法不会对{@link PersonGroupBean}的父结点向上回溯
	 * @param deviceGroupId
	 * @return 人员组ID列表
	 */
	List<Integer> getPersonGroupsPermittedBy(int deviceGroupId);
	/**
	 * 从permit表返回允许在{@code personGroupId}指定的人员组通过的所有设备组({@link DeviceGroupBean})的id<br>
	 * 不排序,不包含重复id,本方法不会对{@code personGroupId}的父结点向上回溯
	 * @param personGroupId
	 * @return 设备组ID列表
	 */
	List<Integer> getDeviceGroupsPermittedBy(int personGroupId);

	/**
	 * 从permit表返回允许在{@code personGroupId}指定的人员组通过的所有设备组({@link DeviceGroupBean})的id<br>
	 * 不排序,不包含重复id
	 * @param personGroupId 为{@code null}返回空表
	 * @return 设备组ID列表
	 */
	List<Integer> getDeviceGroupsPermit(int personGroupId);
	/**
	 * (主动更新机制实现)<br>
	 * 返回 fl_permit.create_time 字段大于指定时间戳( {@code timestamp} )的所有fl_permit记录
	 * @param timestamp
	 * @return 通行权限记录列表
	 */
	public List<PermitBean> loadPermitByUpdate(long timestamp);
	/**
	 * (主动更新机制实现)返回 fl_permit.create_time 字段大于指定时间戳( {@code timestamp} )的所有fl_permit记录<br>
	 * @param timestamp 时间戳,,{@code yyyy-MM-dd}或{@code yyyy-MM-dd HH:mm:ss}或{@code yyyy-MM-dd'T'HH:mm:ss.SSSZ}(ISO8601)格式日期字符串
	 * @return 通行权限记录列表
	 */
	@DeriveMethod(methodSuffix="Timestr")
	public List<PermitBean> loadPermitByUpdate(String timestamp);

    /**
     * 查询{@code where} SQL条件语句指定的记录
     * @param where 'WHERE'开头的SQL条件语句,为{@code null}或空时加载所有记录
     * @param startRow 返回记录的起始行(首行=1,尾行=-1)
     * @param numRows 返回记录条数(小于0时返回所有记录)
     * @return 人员组ID列表
     * @deprecated replaced by {@link #loadPersonGroupIdByWhere(String)}
     */
    public List<Integer> loadPersonGroupByWhere(String where,int startRow, int numRows);

    /**
     * 返回满足{@code where} SQL条件语句的 fl_person_group 记录总数
     * @param where 'WHERE'开头的SQL条件语句,为{@code null}或空时加载所有记录
     * @return 返回满足{@code where} SQL条件语句的 fl_person_group 记录总数
     * @deprecated
     */
    public int countPersonGroupByWhere(String where);
    /**
     * 返回满足{@code where} SQL条件语句的 fl_person_group 记录总数(只统计访问令牌允许访问的记录)
     * @param where 'WHERE'开头的SQL条件语句,为{@code null}或空时加载所有记录
     * @param token 访问令牌
     * @return 返回满足{@code where} SQL条件语句的 fl_person_group 记录总数
     * @since 5.0.0
     */
    @DeriveMethod(methodSuffix="Safe")
    public int countPersonGroupByWhere(String where, Token token);
    /** 
     * 查询{@code where}条件指定的记录
     * @param where 'WHERE'开头的SQL条件语句,为{@code null}或空时加载所有记录
     * @return 返回人员组列表
     * @see #loadPersonGroupByWhere(String,int,int)
     * @deprecated
     */
    public List<Integer> loadPersonGroupIdByWhere(String where);

    /** 
     * 查询{@code where}条件指定的记录(只返回访问令牌允许访问的记录)
     * @param where 'WHERE'开头的SQL条件语句,为{@code null}或空时加载所有记录
     * @return 返回人员组列表
     * @param token 访问令牌
     * @since 5.0.0
     */
    @DeriveMethod(methodSuffix="Safe")
    public List<Integer> loadPersonGroupIdByWhere(String where, Token token);
	/**
	 * 新设备注册,如果设备已经注册则返回注册设备记录<br>
	 * 注册时必须提供设备MAC地址,是否提供序列号,根据应用需要选择
	 * @param newDevice 设备记录,_isNew字段必须为{@code true},{@code id}字段不要指定,数据库会自动分配,保存在返回值中
	 * @return 设备记录
	 * @throws ServiceSecurityException
	 */
	public DeviceBean registerDevice(DeviceBean newDevice) throws ServiceSecurityException;
	/**
	 * (设备端)删除当前设备<br>
	 * 从fl_device表中删除当前设备记录
	 * <br>{@code DEVICE_ONLY}
	 * @param token 设备验证令牌
	 * @throws ServiceSecurityException
	 */
	public void unregisterDevice(Token token)
			throws ServiceSecurityException;
	/**
	 * 设备申请上线,每次调用都会产生一个新的令牌
	 * @param device 上线设备信息，必须提供{@code id, mac, serialNo}字段
	 * @return 设备访问令牌
	 * @throws ServiceSecurityException
	 */
	public Token online(DeviceBean device)
			throws ServiceSecurityException;
	/**
	 * 设备申请离线,删除设备令牌
	 * <br>{@code DEVICE_ONLY}
	 * @param token 当前持有的令牌
	 * @throws ServiceSecurityException
	 */
	public void offline(Token token)	throws ServiceSecurityException;
	/**
	 * 申请人员访问令牌
	 * @param personId 用户ID
	 * @param password 密码
	 * @param isMd5 为{@code false}代表{@code password}为明文,{@code true}指定{@code password}为32位MD5密文(小写)
	 * @return 返回申请的令牌
	 * @throws ServiceSecurityException
	 */
	public Token applyPersonToken(int personId, String password, boolean isMd5)
			throws ServiceSecurityException;
	/**
	 * 释放人员访问令牌
	 * <br>{@code PERSON_ONLY}
	 * @param token 当前持有的令牌
	 * @throws ServiceSecurityException
	 */
	public void releasePersonToken(Token token)
			throws ServiceSecurityException;
	/**
	 * 申请root访问令牌
	 * @param password root用户密码
	 * @param isMd5 为{@code false}代表{@code password}为明文,{@code true}指定{@code password}为32位MD5密文(小写)
	 * @return 返回申请的令牌
	 * @throws ServiceSecurityException
	 */
	public Token applyRootToken(String password, boolean isMd5)
			throws ServiceSecurityException;

	/**
	 * 释放root访问令牌
	 * <br>{@code ROOT_ONLY}
	 * @param token 当前持有的令牌
	 * @throws ServiceSecurityException
	 */
	public void releaseRootToken(Token token)
			throws ServiceSecurityException;
	/**
	 * 申请person/root访问令牌
	 * @param userid 用户ID(为-1时为root)
	 * @param password 用户密码
	 * @param isMd5 为{@code false}代表{@code password}为明文,{@code true}指定{@code password}为32位MD5密文(小写)
	 * @return 返回申请的令牌
	 * @throws ServiceSecurityException
	 * @since 2.1.1
	 */
	Token applyUserToken(int userid, String password, boolean isMd5) throws ServiceSecurityException;

	/**
	 * 释放person/root访问令牌
	 * @param token 要释放的令牌,如果令牌类型非{@link net.gdface.facelog.Token.TokenType#PERSON}或{@link net.gdface.facelog.Token.TokenType#ROOT}则抛出{@link ServiceSecurityException}异常
	 * @throws ServiceSecurityException
	 * @since 2.1.1
	 */
	void releaseUserToken(Token token) throws ServiceSecurityException;

	/**
	 * 验证用户密码是否匹配
	 * @param userId 用户id字符串,root用户id即为{@link CommonConstant#ROOT_NAME}
	 * @param password 用户密码
	 * @param isMd5 为{@code false}代表{@code password}为明文,{@code true}指定{@code password}为32位MD5密文(小写)
	 * @return {@code true}密码匹配
	 */
	public boolean isValidPassword(String userId,String password, boolean isMd5) ;
	/**
	 * 申请一个唯一的命令响应通道(默认有效期)<br>
	 * <br>{@code PERSON_ONLY}
	 * @param token 访问令牌
	 * @return 命令响应通道名
	 */
	public String applyAckChannel(Token token);
	/**
	 * 申请一个唯一的命令响应通道<br>
	 * <br>{@code PERSON_ONLY}
	 * @param duration 通道有效时间(秒) 大于0有效,否则使用默认的有效期
	 * @param token 访问令牌
	 * @return 命令响应通道名
	 */
	@DeriveMethod(methodSuffix="WithDuration")
	public String applyAckChannel(int duration, Token token);
	/**
	 * 申请一个唯一的命令序列号
	 * <br>{@code PERSON_ONLY}
	 * @param token 访问令牌
	 * @return 命令序列号
	 */
	public int applyCmdSn(Token token);
	/**
	 * 判断命令序列号是否有效<br>
	 * 序列号过期或不存在都返回{@code false}
	 * @param cmdSn
	 * @return true if cmdSn is valid
	 */
	public boolean isValidCmdSn(int cmdSn);
	/**
	 * 判断命令响应通道是否有效<br>
	 * 通道过期或不存在都返回{@code false}
	 * @param ackChannel
	 * @return true if ackChannel is valid
	 */
	public boolean isValidAckChannel(String ackChannel);
    /**
	 * 验证设备令牌是否有效
	 * @param token
	 * @return 令牌有效返回{@code true},否则返回{@code false}
	 */
	boolean isValidDeviceToken(Token token);

	/**
	 * 验证人员令牌是否有效
	 * @param token
	 * @return  令牌有效返回{@code true},否则返回{@code false}
	 */
	boolean isValidPersonToken(Token token);

	/**
	 * 验证root令牌是否有效
	 * @param token
	 * @return 令牌有效返回{@code true},否则返回{@code false}
	 */
	boolean isValidRootToken(Token token);
	/**
	 * 验证PERSON/ROOT令牌是否有效
	 * @param token
	 * @return 令牌有效返回{@code true},否则返回{@code false}
	 * @since 2.1.1
	 */
	boolean isValidUserToken(Token token);

	/**
	 * 验证令牌是否有效
	 * @param token
	 * @return 令牌有效返回{@code true},否则返回{@code false}
	 * @since 2.1.1
	 */
	boolean isValidToken(Token token);

	/**
     * 返回redis访问基本参数:<br>
     * <ul>
     * <li>redis服务器地址</li>
     * <li>设备命令通道名</li>
     * <li>人员验证实时监控通道名</li>
     * <li>设备心跳实时监控通道名</li>
     * <li>设备心跳包间隔时间(秒)</li>
     * <li>设备心跳包失效时间(秒)</li>
     * </ul>
     * 参见{@link MQParam}定义
     * @param token 访问令牌
     * @return 参数名-参数值映射
     * @since 2.6.1
     */
    public Map<MQParam,String> getRedisParameters(Token token);
    
	/**
	 * 返回FaceApi RPC服务的配置<br>
	 * 以 SDK VERSION -- host:port 映射形式返回存在的FaceApi服务主机和端口号
	 * @param token 访问令牌
	 * @return SDK VERSION -- host:port 映射
	 */
	public Map<String,String> getFaceApiParameters(Token token);

	/**
     * 返回消息系统基本参数:<br>
     * <ul>
     * <li>redis服务器地址</li>
     * <li>设备命令通道名</li>
     * <li>人员验证实时监控通道名</li>
     * <li>设备心跳实时监控通道名</li>
     * <li>设备心跳包间隔时间(秒)</li>
     * <li>设备心跳包失效时间(秒)</li>
     * <li>服务端支持的消息系统类型</li>
     * <li>消息系统连接参数(json)字符串</li>
     * </ul>
     * 参见{@link MQParam}定义
     * @param token 访问令牌
     * @return 参数名-参数值映射
     */
    public Map<MQParam,String> getMessageQueueParameters(Token token);

	/**
	 * 根据任务名返回redis队列名
	 * @param task 任务名
	 * @param token 访问令牌
	 * @return 返回redis队列名,队列不存在则返回{@code null}
	 */
	public String taskQueueOf(String task, Token token);
	
	/**
	 * 返回sdk任务队列名
	 * @param task 任务名,可选值:{@link CommonConstant#TASK_FACEAPI_BASE},{@link CommonConstant#TASK_REGISTER_BASE}
	 * @param sdkVersion sdk版本号
	 * @param token 访问令牌
	 * @return  返回sdk任务队列名，参数错误返回{@code null}
	 */
	public String sdkTaskQueueOf(String task, String sdkVersion, Token token);
	/**
	 * (异步)执行cmdpath指定的设备命令<br>
	 * <br>{@code PERSON_ONLY}
	 * @param target 命令目标设备/设备组id
	 * @param group target中的元素是否为设备组id
	 * @param cmdpath 设备命令的dtalk路径
	 * @param jsonArgs 设备命令参数(JSON)
	 * @param ackChannel 设备命令响应频道,不需要接收命令响应时设置为{@code null}
	 * @param token 访问令牌
	 * @return 以map形式返回收到命令的客户端数目和命令序列号,如{"client":25,"cmdSn":12309898}
	 */
	String runCmd(List<Integer>target, boolean group, String cmdpath, String jsonArgs, String ackChannel, Token token);
	/**
	 * (异步)执行cmdpath指定的任务<br>
	 * <br>{@code PERSON_ONLY}
	 * @param taskQueue 任务队列名称
	 * @param cmdpath 设备命令的dtalk路径
	 * @param jsonArgs 设备命令参数(JSON)
	 * @param ackChannel 设备命令响应频道,不需要接收命令响应时设置为{@code null}
	 * @param token 访问令牌
	 * @return 成功提交任务返回命令序列号,否则返回{@code null}
	 */
	Integer runTask(String taskQueue, String cmdpath, String jsonArgs, String ackChannel, Token token);
	/**
	 * (同步)执行cmdpath指定的任务<br>
	 * 因为设备命令响应对象(gu.dtalk.Ack)为泛型类,无法用作传输类型,所以将Ack转换为JSON字符串返回
	 * @param taskQueue 任务队列名称
	 * @param cmdpath 设备命令的dtalk路径
	 * @param jsonArgs 设备命令参数(JSON)
	 * @param timeoutSecs 任务超时(秒),等待任务执行的最长时间,为0使用默认值
	 * @param token 访问令牌
	 * @return 转换为JSON字符串的设备命令响应对象(gu.dtalk.Ack),如果执行超时返回{@code null}
	 */
	public String runTaskSync(String taskQueue, String cmdpath, String jsonArgs, int timeoutSecs, Token token);
	/**
	 * 查询 {@code table}表的名为{@code column}类型为字段数据,将字段值转为{@link String}类型返回
	 * @param table 数据库表名
     * @param column 有效的table表字段名或{@link LogLightBean} 字段名
     * @param where 'where'起始的SQL 查询条件语句,可为{@code null}
	 * @return 返回不重复的字段值列表
	 */
	List<String> loadDistinctStringColumn(String table, String column, String where);
	/**
	 * 查询 {@code table}表的名为{@code column}类型为{@link Integer}类型的字段数据
	 * @param table 数据库表名
     * @param column 有效的table表字段名或{@link LogLightBean} 字段名
     * @param where 'where'起始的SQL 查询条件语句,可为{@code null}
	 * @return 返回不重复的字段值列表
	 */
	List<Integer> loadDistinctIntegerColumn(String table, String column, String where);

	/**
	 * 返回指定的参数,如果参数没有定义则返回{@code null}<br>
	 * 非root令牌只能访问指定范围的参数,否则会抛出异常<br>
	 * root令牌不受限制<br>
	 * @param key
	 * @param token 访问令牌
	 * @return 返回{@code key}指定的参数值
	 */
	public String getProperty(String key,Token token);
	
	/**
	 * 返回{@code prefix}为前缀的所有参数
	 * 非root令牌只能访问指定范围的参数,否则会抛出异常<br>
	 * root令牌不受限制<br>
	 * @param prefix
	 * @param token
	 * @return 参数名-参数值映射
	 */
	public Map<String, String> getProperties(String prefix, Token token);

	/**
	 * 获取服务的所有配置参数
	 * <br>{@code ROOT_ONLY}
	 * @param token 访问令牌
	 * @return 参数名-参数值映射
	 */
	public Map<String,String> getServiceConfig(Token token);
	/**
	 * 修改/增加指定的配置参数
	 * <br>{@code ROOT_ONLY}
	 * @param key 参数名
	 * @param value 参数值
	 * @param token 访问令牌
	 */
    public void setProperty(String key,String value,Token token);
    /**
     * 修改一组配置参数
	 * <br>{@code ROOT_ONLY}
     * @param config 参数名-参数值对
     * @param token 访问令牌
     */
    public void setProperties(Map<String,String> config,Token token);
	/**
	 * 配置参数持久化<br>
	 * 保存修改的配置到自定义配置文件
	 * <br>{@code ROOT_ONLY}
	 * @param token 访问令牌
	 */
    public void saveServiceConfig(Token token);
    
	/**
	 * 返回当前系统时间(ISO8601格式字符串)
	 * @return ISO8601格式字符串
	 */
	public String iso8601Time();
	
	/**
	 * 为指定的目标(用户组/设备组/用户/设备)创建临时密码<br>
	 * 如果有效期为{@code null}则使用默认值(10分钟)
	 * <br>{@code PERSON_ONLY}
	 * 返回6位数字临时密码
	 * @param targetId 临时密码应用的目标ID
	 * @param targetType 目标ID的类型
	 * @param expiryDate 临时密码的有效期失效日期,{@code yyyy-MM-dd}或{@code yyyy-MM-dd HH:mm:ss}或{@code yyyy-MM-dd'T'HH:mm:ss.SSSZ}(ISO8601)格式日期字符串
	 * @param token 访问令牌
	 * @return 临时密码
	 */
	public String createTempPwd(int targetId, TmpPwdTargetType targetType, String expiryDate, Token token);
	/**
	 * 为指定的目标(用户组/设备组/用户/设备)创建临时密码<br>
	 * <br>{@code PERSON_ONLY}
	 * 返回6位数字临时密码
	 * @param targetId 临时密码应用的目标ID
	 * @param targetType 目标ID的类型
	 * @param duration 有效期持续时间(分钟),必须 > 0
	 * @param token 访问令牌
	 * @return 临时密码
	 */
	@DeriveMethod(methodSuffix="WithDuration")
	public String createTempPwd(int targetId, TmpPwdTargetType targetType, int duration, Token token);

	/**
	 * 获取临时密码对应的{@link TmpwdTargetInfo}对象<br>
	 * 如果临时密码对于当前设备无效,则返回空
	 * <br>{@code DEVICE_ONLY}
	 * 
	 * @param pwd 临时密码
	 * @param token 设备访问令牌
	 * @return {@link TmpwdTargetInfo}对象
	 */
	public TmpwdTargetInfo getTargetInfo4PwdOnDevice(String pwd, Token token);

	/**
	 * (1:N)识别照片中的人脸,识别后判断是否可以在指定设备通行<br>
	 * 该方法只有在服务端有人脸识别算法支持时才有效,see also {@link #getFaceApiParameters(Token)},
	 * 调用中抛出的异常被封装在{@code net.gdface.thrift.exception.ServiceRuntimeException}再装入ServiceRuntimeException(type为ExceptionType.FACEAPI_ERROR) 抛出,
	 * @param imageData 人脸图像数据(jpg,png,bmp),有且只有一张人脸
	 * @param threshold 人脸特征比对的相似度阀值,为{@code null}使用默认值
	 * @param group 特征分组,为0时,为全局特征搜索,否则只搜索特征分组掩码({@code CodeBean.group})匹配group的特征<br>
	 *                                <ul>
	 *                                <li>0x02 男性特征分组(只匹配性别为男性[fl_person.sex字段]的人脸特征)</li>
	 *                                <li>0x04 女性特征分组(只匹配性别为女性[fl_person.sex字段]的人脸特征)</li>
	 *                                </ul>
	 * @param deviceId 设备ID
	 * @param searchInPermited 为{@code true}时只在设备上允许通行的用户人脸中进行匹配,否则在所有人脸特征数据中匹配
	 * @return 如果识别的用户允许在指定设备通过返回用户ID,否则返回无效ID(-1)
	 */
	public int faceRecognizePersonPermitted(byte[] imageData, Float threshold, int group, int deviceId, boolean searchInPermited);

	/**
	 * 人脸锁(嵌入式)设备唤醒启动<br>
	 * 执行设备注册,上线，并获取可以在该设备通行的所有人员ID及特征码ID,将所有必要的结果数据封装在{@link LockWakeupResponse}中返回
	 * @param deviceBean 设备记录,_isNew字段必须为{@code true},{@code id}字段不要指定,数据库会自动分配,保存在返回值中
	 * @param ignoreSchedule 在获取指定设备上允许通行的所有人员记录时,是否忽略时间过滤器(fl_permit.schedule字段)的限制
	 * @param sdkVersion 获取人员人脸特征记录时需要的算法(SDK)版本号
	 * @return 将获取数据封装在 LockWakeupPackge 对象返回
	 * @throws ServiceSecurityException
	 */
	public LockWakeupResponse lockWakeup(DeviceBean deviceBean, boolean ignoreSchedule, String sdkVersion) throws ServiceSecurityException;
	/**
	 * 人员组设备级初始化<gr>
	 * 根据输入的{@link TopGroupInfo}对象创建顶级组其下子节点,并完成初始的通行权限配置<br>
 	 * <br>{@code ROOT_ONLY}
	 * @param groupInfo 用于初始化的顶级组描述信息对象
	 * @return 顶级人员组id
	 */
	public int initTopGroup(TopGroupInfo groupInfo, Token token);
	/**
	 * 通过指定路径查找匹配的人员组/设备组ID<br>
	 * @param tablename 表名,必须为'fl_device_group'或'fl_person_group'
	 * @param path 以'/'分隔的级联组名组成的路径,如 '1楼/2单元/102室'
	 * @return 匹配路径的所有人员组/设备组ID列表,没有找到返回空表
	 */
	public List<Integer> getGroupIdsByPath(String tablename, String path);
	/**
	 * 返回指定人员组/设备组的全路径<br>
	 * @param tablename 表名,必须为'fl_device_group'或'fl_person_group'
	 * @param groupId 人员组/设备组ID
	 * @return 代表设备组/人员组的以'/'分隔的级联组名组成的路径,如 '1楼/2单元/102室'
	 */
	public String pathOf(String tablename, int groupId);
	/**
	 * 表字段模糊匹配查询<br>
	 * 根据指定的匹配条件({@code pattern})对{@code tablename}指定的表字段模糊匹配<br>
	 * 比如 '1102'匹配'1楼/1单元/102室'<br>
	 * {@code matchType}为{@code null}时,使用的默认匹配策略与{@code tablename}和{@code column}相关
	 * <pre>
	 * tablename,column值--默认匹配策略
	 * fl_device.name--支持通配符的字符串比较匹配{@link StringMatchType#WILDCARD_MATCH}
	 * fl_device.mac--支持通配符的字符串比较匹配{@link StringMatchType#WILDCARD_MATCH}
	 * fl_device_group--右侧数字模糊匹配{@link StringMatchType#DIGIT_FUZZY_RIGHT_MATCH}
	 * fl_person_group--右侧数字模糊匹配{@link StringMatchType#DIGIT_FUZZY_RIGHT_MATCH}
	 * </pre>
	 * 
	 * 匹配标志{@code matchFlags}为下列常量的组合:<br>
	 * <pre>
	 * FM_CASSENSITIVE(0x02) 大小写不敏感
	 * FM_MULTILINE(0x08) 正则表达式多行匹配模式
	 * FM_RECURSIVE(0x200) 递归查询父节点组ID下的记录,父节点组ID小于等于0时忽略
	 * </pre>
	 *
	 * @param tablename 表名,必须为'fl_device_group'或'fl_person_group','fl_device'
	 * @param column 指定模糊搜索的表字段名,可为{@code null},{@code tablename}为'fl_device'时必须为'name'或'mac'<br>
	 * @param pattern 模糊匹配条件,如'102'
	 * @param matchType 模糊匹配策略,为{@code null}使用默认值
	 * @param matchFlags 匹配标志
	 * @param parentGroupId 父节点组ID(小于等于0时忽略),限定查询此节点之下的记录
	 * @param maxMatchCount 要求的最多匹配结果,如果返回的匹配结果超过此值则抛出异常,小于等于0时忽略
	 * @return 匹配结果列表,没有找到匹配记录时返回空表
	 * @throws FuzzyMatchCountExceedLimitException 返回的匹配结果超过{@code maxMatchCount}
	 */
	public List<MatchEntry> fuzzySearch(String tablename, String column, String pattern, StringMatchType matchType, int matchFlags, int parentGroupId, int maxMatchCount) 
			throws FuzzyMatchCountExceedLimitException;
	
	/**
	 * 人员表字段模糊匹配查询<br>
 	 * {@code PERSON_ONLY}<br>
	 * 根据指定的匹配条件({@code pattern})对{@code tablename}指定的表字段模糊匹配<br>
	 * {@code matchType}为{@code null}时,使用的默认匹配策略与{@code column}相关
	 * <pre>
	 * column值--默认匹配策略
	 * name--支持通配符的字符串比较匹配{@link StringMatchType#WILDCARD_MATCH}
	 * papsers_num--支持通配符的字符串比较匹配{@link StringMatchType#WILDCARD_MATCH}
	 * mobile_phone--数字模糊匹配{@link StringMatchType#DIGIT_FUZZY_MATCH}
	 * </pre>
	 * 
	 * 匹配标志{@code matchFlags}为下列常量的组合:<br>
	 * <pre>
	 * FM_CASSENSITIVE(0x02) 大小写不敏感
	 * FM_MULTILINE(0x08) 正则表达式多行匹配模式
	 * FM_RECURSIVE(0x200) 递归查询父节点组ID下的记录,父节点组ID小于等于0时忽略
	 * </pre>
	 * 
	 * @param column 指定模糊搜索的表字段名,可为'name','papsers_num','mobile_phone'<br>
	 * @param pattern 模糊匹配条件,如'102'
	 * @param matchType 模糊匹配策略,为{@code null}使用默认值
	 * @param matchFlags 匹配标志
	 * @param parentGroupId 父节点组ID(小于等于0时忽略),限定查询此节点之下的记录
	 * @param maxMatchCount 要求的最多匹配结果,如果返回的匹配结果超过此值则抛出异常,小于等于0时忽略
	 * @param token 访问令牌
	 * @return 匹配结果列表,没有找到匹配记录时返回空表
	 * @throws FuzzyMatchCountExceedLimitException 返回的匹配结果超过{@code maxMatchCount}
	 */
	List<MatchEntry> fuzzySearchPerson(String column, String pattern, StringMatchType matchType, int matchFlags, int parentGroupId, int maxMatchCount, Token token)
			throws FuzzyMatchCountExceedLimitException;
	/**
	 * 返回服务版本号
	 * @return 服务版本号
	 */
	public String version();
	/**
	 * 返回服务版本的详细信息<br>
	 * <ul>
	 * <li>{@code VERSION} -- 服务版本号</li>
	 * <li>{@code SCM_REVISION} -- GIT修订版本号</li>
	 * <li>{@code SCM_BRANCH} -- GIT分支</li>
	 * <li>{@code TIMESTAMP} -- 时间戳</li>
	 * </ul>
	 * 
	 * @return 服务版本的详细信息(参数名-参数值映射)
	 */
	public Map<String, String> versionInfo();
	/**
	 * 是否为本地实现
	 * @return true if local implementation
	 */
	public boolean isLocal();

	

	

	

	

	

	

	

	

	

	

	

	

	

	

}