package net.gdface.facelog;

import java.util.List;

import net.gdface.annotation.CodegenLength;
import net.gdface.annotation.CodegenRequired;
import net.gdface.facelog.db.DeviceBean;

/**
 * 人脸锁(嵌入)设备唤醒响应数据包
 * @author guyadong
 *
 */
public class LockWakeupResponse {
	@CodegenRequired@CodegenLength(max=32,prealloc=true)
	private String iso8601Timestamp;
	@CodegenRequired
	private DeviceBean deviceBean;
	@CodegenRequired
	private Token token;
	@CodegenRequired
	private List<PersonSummary>persons;
	@CodegenRequired
	private List<TmpPassword> tmpPasswords;
	public LockWakeupResponse() {
	}

	/**
	 * @return iso8601Timestamp
	 */
	public String getIso8601Timestamp() {
		return iso8601Timestamp;
	}

	/**
	 * @param iso8601Timestamp 要设置的 iso8601Timestamp
	 */
	public void setIso8601Timestamp(String iso8601Timestamp) {
		this.iso8601Timestamp = iso8601Timestamp;
	}

	/**
	 * @return deviceBean
	 */
	public DeviceBean getDeviceBean() {
		return deviceBean;
	}

	/**
	 * @param deviceBean 要设置的 deviceBean
	 */
	public void setDeviceBean(DeviceBean deviceBean) {
		this.deviceBean = deviceBean;
	}

	/**
	 * @return token
	 */
	public Token getToken() {
		return token;
	}

	/**
	 * @param token 要设置的 token
	 */
	public void setToken(Token token) {
		this.token = token;
	}

	/**
	 * @return persons
	 */
	public List<PersonSummary> getPersons() {
		return persons;
	}

	/**
	 * @param persons 要设置的 persons
	 */
	public void setPersons(List<PersonSummary> persons) {
		this.persons = persons;
	}

	/**
	 * @return tmpPasswords
	 */
	public List<TmpPassword> getTmpPasswords() {
		return tmpPasswords;
	}

	/**
	 * @param tmpPasswords 要设置的 tmpPasswords
	 */
	public void setTmpPasswords(List<TmpPassword> tmpPasswords) {
		this.tmpPasswords = tmpPasswords;
	}
}
