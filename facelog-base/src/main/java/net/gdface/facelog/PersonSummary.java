package net.gdface.facelog;

import java.util.List;

/**
 * 人员(摘要)信息<br>
 * 包含对应人员的特征码ID(MD5),具体所属SDK版本号由使用者决定
 * @author guyadong
 *
 */
public class PersonSummary{
	/** 人员ID */
	private int personId;
	/**
	 * 人员ID对应的特征码ID(MD5)列表
	 */
	private List<String> featureIds;
	/**
	 * 验证有效期限(超过期限不能通过验证),为NULL永久有效
	 * fl_person.expiry_date字段
	 */
	private String expiryDate;
	/**
	 * (JSON格式)扩展字段
	 */
	private String extjson;
	public PersonSummary() {
		super();
	}
	public PersonSummary(int personId, List<String> featureIds, String expiryDate, String extjson) {
		super();
		this.personId = personId;
		this.featureIds = featureIds;
		this.expiryDate = expiryDate;
		this.extjson = extjson;
	}
	/**
	 * @return personId
	 */
	public int getPersonId() {
		return personId;
	}
	/**
	 * @param personId 要设置的 personId
	 */
	public void setPersonId(int personId) {
		this.personId = personId;
	}
	/**
	 * @return featureIds
	 */
	public List<String> getFeatureIds() {
		return featureIds;
	}
	/**
	 * @param featureIds 要设置的 featureIds
	 */
	public void setFeatureIds(List<String> featureIds) {
		this.featureIds = featureIds;
	}
	/**
	 * @return expiryDate
	 */
	public String getExpiryDate() {
		return expiryDate;
	}
	/**
	 * @param expiryDate 要设置的 expiryDate
	 */
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	/**
	 * @return extjson
	 */
	public String getExtjson() {
		return extjson;
	}
	/**
	 * @param extjson 要设置的 extjson
	 */
	public void setExtjson(String extjson) {
		this.extjson = extjson;
	}
}