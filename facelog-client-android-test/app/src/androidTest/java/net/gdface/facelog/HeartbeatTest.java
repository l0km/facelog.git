package net.gdface.facelog;

import android.support.test.runner.AndroidJUnit4;

import static org.junit.Assert.*;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gu.simplemq.MessageQueueFactorys;
import gu.simplemq.exceptions.SmqUnsubscribeException;
import net.gdface.facelog.client.IFaceLogClient;
import net.gdface.facelog.client.RefreshTokenDecorator;
import net.gdface.facelog.client.TokenHelper;
import net.gdface.facelog.hb.DynamicChannelListener;
import net.gdface.facelog.mq.DeviceHeartbeatListener;
import net.gdface.facelog.mq.DeviceHeartdbeatPackage;
import net.gdface.facelog.thrift.IFaceLogThriftClient;
import net.gdface.thrift.ClientFactory;

/**
 * 心跳包测试
 * @author guyadong
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(AndroidJUnit4.class)
public class HeartbeatTest implements ChannelConstant {

	public static final Logger logger = LoggerFactory.getLogger(HeartbeatTest.class);

	private static IFaceLogClient facelogClient;
	private static Token rootToken;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		// 创建服务实例
		facelogClient = ClientFactory.builder()
				.setHostAndPort("127.0.0.1", DEFAULT_PORT)
				.setDecorator(RefreshTokenDecorator.makeDecoratorFunction(TokenHelperTestImpl.INSTANCE))
				.build(IFaceLogThriftClient.class, IFaceLogClient.class);
		// 申请令牌
		rootToken = facelogClient.applyRootToken("root", false);
		facelogClient.setTokenHelper(TokenHelperTestImpl.INSTANCE)
				.startServiceHeartbeatListener(rootToken, true);
		facelogClient.initMQDefaultFactory(rootToken);
	}
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		facelogClient.releaseRootToken(rootToken);
	}
	/**
	 * 设备端发送心跳包测试
	 * @throws InterruptedException
	 */
	@Test
	public void test1SendHB() throws InterruptedException {

		System.out.println("Heartbeat thead start");
		try {
			facelogClient.makeHeartbeat(12345, rootToken)
					/** 间隔2秒发送心跳，重新启动定时任务 */
					.setInterval(2, TimeUnit.SECONDS)
					.start();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	/**
	 * 管理端心跳包监控测试
	 * @throws InterruptedException
	 */
	@Test
	public void test2HBMonitor() throws InterruptedException{
		DeviceHeartbeatListener hbAdapter = new DeviceHeartbeatListener(){
			@Override
			public void onSubscribe(DeviceHeartdbeatPackage t) throws SmqUnsubscribeException {
				// 显示收到的心跳包
				logger.info(t.toString());
			}};

		DynamicChannelListener<DeviceHeartdbeatPackage> monitor = new DynamicChannelListener<>(hbAdapter,
				DeviceHeartdbeatPackage.class,
				facelogClient.getDynamicParamSupplier(MQParam.HB_MONITOR_CHANNEL,rootToken),
				MessageQueueFactorys.getDefaultFactory());
		facelogClient.addServiceEventListener(monitor);
		monitor.start();
		/** 40秒后结束测试 */
		Thread.sleep(300*1000);
	}
	public static class TokenHelperTestImpl extends TokenHelper {
		final static TokenHelperTestImpl INSTANCE = new TokenHelperTestImpl();
		public TokenHelperTestImpl() {
		}

		@Override
		public String passwordOf(int id) {
			return "guyadong";
		}

		@Override
		public boolean isHashedPwd() {
			return false;
		}

	}
}
