/*
 * Copyright 2020 zytech
 * All rights reserved.
 *
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <thread>
#include <memory>
#include "erpc_concurrency_server.h"
#include "erpc_server_tcp_transport.h"
#include "ThreadPool.h"
#include "raii.h"

using namespace erpc;
using namespace gdface;
static std::shared_ptr<ThreadPool> thread_pool;
////////////////////////////////////////////////////////////////////////////////
// Code
////////////////////////////////////////////////////////////////////////////////
ConcurrencyServer::ConcurrencyServer(size_t concurrency)
: m_isServerOn(true)
, m_concurrency(concurrency ? concurrency : std::thread::hardware_concurrency())
{
	thread_pool = std::make_shared<ThreadPool>(m_concurrency);
}

void ConcurrencyServer::disposeBufferAndCodec(Codec *codec)
{
    if (codec)
    {
        if (codec->getBuffer())
        {
            m_messageFactory->dispose(codec->getBuffer());
        }
        m_codecFactory->dispose(codec);
    }
}

erpc_status_t ConcurrencyServer::runInternal(void)
{
    MessageBuffer buff;
    Codec *codec = NULL;

    // Handle the request.
    message_type_t msgType;
    uint32_t serviceId;
    uint32_t methodId;
    uint32_t sequence;

    erpc_status_t err = runInternalBegin(&codec, buff, msgType, serviceId, methodId, sequence);
    if (!err)
    {
       	err = runInternalEnd(codec, msgType, serviceId, methodId, sequence);
       	if (err)
		{
       		printf("runInternalEnd err=%d\n",err);
		}
    }else{
    	printf("runInternalBegin err=%d\n",err);
    }

    return err;
}

erpc_status_t ConcurrencyServer::runInternalBegin(Codec **codec, MessageBuffer &buff, message_type_t &msgType,
                                             uint32_t &serviceId, uint32_t &methodId, uint32_t &sequence)
{
    if (m_messageFactory->createServerBuffer())
    {
        buff = m_messageFactory->create();
        if (!buff.get())
        {
            return kErpcStatus_MemoryError;
        }
    }

    // Receive the next invocation request.
    erpc_status_t err = m_transport->receive(&buff);
    if (err)
    {
        // Dispose of buffers.
        if (buff.get())
        {
            m_messageFactory->dispose(&buff);
        }
        return err;
    }

#if ERPC_MESSAGE_LOGGING
    err = logMessage(&buff);
    if (err)
    {
        // Dispose of buffers.
        if (buff.get())
        {
            m_messageFactory->dispose(&buff);
        }
        return err;
    }
#endif

    *codec = m_codecFactory->create();
    if (!*codec)
    {
        m_messageFactory->dispose(&buff);
        return kErpcStatus_MemoryError;
    }

    (*codec)->setBuffer(buff);

    err = readHeadOfMessage(*codec, msgType, serviceId, methodId, sequence);
    if (err)
    {
        // Dispose of buffers and codecs.
        disposeBufferAndCodec(*codec);
    }
    return err;
}

erpc_status_t ConcurrencyServer::runInternalEnd(Codec *codec, message_type_t msgType, uint32_t serviceId, uint32_t methodId,
                                           uint32_t sequence)
{
	gdface::raii raii_socket(
			[&]() {
		// server端发送结束关闭client socket
		reinterpret_cast<ServerTCPTransport*>(m_transport)->closeClientSocket();
	});
    erpc_status_t err = processMessage(codec, msgType, serviceId, methodId, sequence);

    if (err)
    {
        // Dispose of buffers and codecs.
        disposeBufferAndCodec(codec);
        return err;
    }

    if (msgType != kOnewayMessage)
    {

#if ERPC_MESSAGE_LOGGING
        err = logMessage(codec->getBuffer());
        if (err)
        {
            // Dispose of buffers and codecs.
            disposeBufferAndCodec(codec);
            return err;
        }
#endif
        err = m_transport->send(codec->getBuffer());
    }

    // Dispose of buffers and codecs.
    disposeBufferAndCodec(codec);

    return err;
}

erpc_status_t ConcurrencyServer::run(void)
{
	// 启动并发处理线程
	for(int i = 0 ; i < m_concurrency ; ++i)
	{
		thread_pool->enqueue([&]() {
			while (m_isServerOn)
			{
				runInternal();
			}
		});
	}
	std::unique_lock<std::mutex>lk(m_mut);
	m_data_cond.wait(lk,[this]{return !this->m_isServerOn;});
    return kErpcStatus_Success;
}

#if ERPC_NESTED_CALLS
erpc_status_t ConcurrencyServer::run(RequestContext &request)
{
    erpc_status_t err = kErpcStatus_Success;
    while (!err && m_isServerOn)
    {
        MessageBuffer buff;
        Codec *codec = NULL;

        // Handle the request.
        message_type_t msgType;
        uint32_t serviceId;
        uint32_t methodId;
        uint32_t sequence;

        erpc_status_t err = runInternalBegin(&codec, buff, msgType, serviceId, methodId, sequence);

        if (err)
        {
            return err;
        }

        if (msgType == kReplyMessage)
        {
            if (sequence == request.getSequence())
            {
                // Swap the received message buffer with the client's message buffer.
                request.getCodec()->getBuffer()->swap(&buff);
                codec->setBuffer(buff);
            }

            // Dispose of buffers and codecs.
            disposeBufferAndCodec(codec);

            if (sequence != request.getSequence())
            {
                // Ignore message
                continue;
            }

            return kErpcStatus_Success;
        }
        else
        {
            err = runInternalEnd(codec, msgType, serviceId, methodId, sequence);
        }
    }
    return err;
}
#endif

erpc_status_t ConcurrencyServer::poll(void)
{
    if (m_isServerOn)
    {
        if (m_transport->hasMessage())
        {
            return runInternal();
        }
        else
        {
            return kErpcStatus_Success;
        }
    }
    return kErpcStatus_ServerIsDown;
}

void ConcurrencyServer::stop(void)
{
	std::lock_guard<std::mutex>lk(m_mut);
    m_isServerOn = false;
    m_data_cond.notify_one();
}
