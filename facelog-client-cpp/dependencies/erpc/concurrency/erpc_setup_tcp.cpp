/*
 * Copyright 2020 zytech
 * All rights reserved.
 *
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "erpc_setup_tcp.h"
#include "erpc_manually_constructed.h"
#include "erpc_tcp_transport.h"

using namespace erpc;

////////////////////////////////////////////////////////////////////////////////
// Variables
////////////////////////////////////////////////////////////////////////////////

static ManuallyConstructed<TCPTransport> s_transport;

////////////////////////////////////////////////////////////////////////////////
// Code
////////////////////////////////////////////////////////////////////////////////

erpc_transport_t erpc_transport_tcp_init(const char *host, uint16_t port, bool isServer)
{
    s_transport.construct(host,port,isServer);
    erpc_status_t err = s_transport->open();
	if (err)
	{
		return NULL;
	}
    return reinterpret_cast<erpc_transport_t>(s_transport.get());
}

void erpc_transport_tcp_deinit()
{
	s_transport->close();
}