/*
 * Copyright (c) 2020, ZYtech, Inc.
 * All rights reserved.
 *
 * author :guyadong
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef _ERPC_SETUP_DUAL_SERIAL_H_
#define _ERPC_SETUP_DUAL_SERIAL_H_

#include "erpc_transport_setup.h"
/*!
 * @{
 * @brief erpc::DualSerialTransport类的标准C封装
 * @file
 */


////////////////////////////////////////////////////////////////////////////////
// API
////////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

//! @name Host PC serial port transport setup
//@{

/*!
 * @brief Create a host PC serial port transport.
 *
 * Create a host PC serial port transport instance.
 *
 * @param[in] portName Port name.
 * @param[in] baudRate Baud rate.
 *
 * @return Status of init function.
 */
erpc_status_t erpc_transport_dual_serial_init(const char *portName, long baudRate);
/*!
 * @brief Deinitialize dual serial transport.
 *
 * This function deinitializes the dual serial transport.
 */
erpc_status_t erpc_transport_dual_serial_deinit(void);
/*!
 * @brief Get a host PC serial port transport instance.
 *
 * @param[in] isServer True when need instance for server side application.
 *
 * @return Return NULL or erpc_transport_t instance pointer.
 */
erpc_transport_t erpc_transport_dual_serial_get(bool isServer);

//@}

#ifdef __cplusplus
}
#endif

/*! @} */

#endif // _ERPC_SETUP_DUAL_SERIAL_H_
