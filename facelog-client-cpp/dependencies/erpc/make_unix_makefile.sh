#!/bin/bash
# 基于 cmake 的 facelogclient 编译脚本(支持交叉编译)
# Optional Environment Variables: 
#        TOOLCHAIN_FILE 指定交叉编译的工具链文件
#        PREFIX 安装路径
#        PROJECT_FOLDER cmake 生成的工程文件(Makefile)文件夹
#        BUILD_TYPE 编译版本类型(DEBUG|RELEASE),默认 DEBUG
#        OUTPUT_DEBUG_LOG 是否输出调试日志,ON/OFF

sh_folder=$(cd "$(dirname $0)"; pwd -P)
folder_name=$(basename $sh_folder) 
# 定义编译的版本类型(DEBUG|RELEASE)
build_type=DEBUG
output_debug_log=ON
typeset -u arg1=$BUILD_TYPE
[ "$arg1" = "DEBUG"   ] && build_type=$arg1
[ "$arg1" = "RELEASE" ] && build_type=$arg1 && output_debug_log=OFF
[ -n "$OUTPUT_DEBUG_LOG" ] && output_debug_log=$OUTPUT_DEBUG_LOG
echo build_type=$build_type
echo output_debug_log=$output_debug_log
pushd $sh_folder/..

toolchain=
[ -n "$TOOLCHAIN_FILE" ] && toolchain="-DCMAKE_TOOLCHAIN_FILE=$TOOLCHAIN_FILE"

[ -n "$PREFIX" ] && install_prefix="$PREFIX"
[ -z "$PREFIX" ] && install_prefix=$sh_folder/../dist/erpc-$(g++ -dumpmachine)


[ -n "$PROJECT_FOLDER" ] && prj_folder="$PROJECT_FOLDER"
[ -z "$PROJECT_FOLDER" ] && prj_folder=$sh_folder/../$folder_name.gnu

[ -d $prj_folder ] && rm -fr $prj_folder
mkdir -p $prj_folder

pushd $prj_folder

echo "creating $(arch) Project for gnu ..."
cmake "$sh_folder" -G "Eclipse CDT4 - Unix Makefiles" -DCMAKE_BUILD_TYPE=$build_type \
	-DCMAKE_INSTALL_PREFIX=$install_prefix \
	$toolchain \
	-DOUTPUT_DEBUG_LOG=$output_debug_log

# OUTPUT_DEBUG_LOG 是否输出DEBUG日志

#cmake --build $prj_folder --target install -- -j8

popd
popd

