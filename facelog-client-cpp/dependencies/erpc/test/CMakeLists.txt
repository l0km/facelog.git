#cmake file for test
#author:guyadong
#created:2020/11/17

#定义公共代码位置   
set( COMMONS_SOURCE_DIR ${CMAKE_CURRENT_LIST_DIR}/../../common_source_cpp)
if(NOT EXISTS ${COMMONS_SOURCE_DIR}/CMakeLists.txt )
	message( FATAL_ERROR "Not exists or Empty folder: ${COMMONS_SOURCE_DIR}" )	
endif()
# 添加公共代码子项目　common_source
add_subdirectory( ${COMMONS_SOURCE_DIR} common_source_cpp )

##find_package(Erpc MODULE REQUIRED)
################创建 erpc_dual_test 项目用于 双行串行传输测试#################
aux_source_directory(${CMAKE_CURRENT_LIST_DIR} _DUAL_SOURCES)
add_executable(erpc_dual_test erpc_dual_test.cpp ${_DUAL_SOURCES} )
target_include_directories(erpc_dual_test PRIVATE ${CMAKE_CURRENT_LIST_DIR} )
target_link_libraries(erpc_dual_test erpc common_source) 
if(CMAKE_SYSTEM_NAME MATCHES "Linux")
	target_link_libraries(erpc_dual_test -pthread)
endif()
####################################################################

##################################
set_target_properties(erpc_dual_test PROPERTIES FOLDER "test")
set_property(TARGET erpc_dual_test APPEND PROPERTY COMPILE_OPTIONS $<$<CXX_COMPILER_ID:MSVC>:/wd4275 /wd4251>
	$<$<CXX_COMPILER_ID:GNU>:-Wno-format-security>)
################安装脚本#################
# 安装exe
install(TARGETS erpc_dual_test RUNTIME DESTINATION ${RUNTIME_INSTALL_DIR} )
