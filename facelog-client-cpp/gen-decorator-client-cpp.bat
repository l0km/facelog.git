@rem 生成IFaceLog接口的client实现代码(CPP)
@set sh_folder=%~dp0
@pushd %sh_folder%
@set OUT_FOLDER=src\client
@if exist "%OUT_FOLDER%" del %OUT_FOLDER%\*.cpp %OUT_FOLDER%\*.h %OUT_FOLDER%\*.md
call mvn com.gitee.l0km:codegen-thrift-maven-plugin:generate@client
@popd