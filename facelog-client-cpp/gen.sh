#!/bin/bash
sh_folder=$(cd "$(dirname $0)"; pwd -P)
pushd $sh_folder
./erpc_c.sh || exit
./erpc_c_mini.sh || exit
./gen-erpc-proxy.sh || exit
./gen-erpc-proxy-mini.sh || exit
./gen-client-cpp.sh || exit
./gen-decorator-client-cpp.sh || exit
popd
