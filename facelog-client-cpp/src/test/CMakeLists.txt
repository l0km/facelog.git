#cmake file for test
#author:guyadong
#created:2018/05/10
# 定义 /MT,MD 后缀
set(_static_postfix "")
if(MSVC)
	if(Boost_USE_STATIC_RUNTIME)
		set(_static_postfix _mt)
	else()
		set(_static_postfix _md)
	endif(Boost_USE_STATIC_RUNTIME)
endif(MSVC)
set(_linke_libs facelogclient${_static_postfix} common_source)
set(_test_targets)
add_executable(connect_test connect_test.cpp )
target_link_libraries(connect_test ${_linke_libs})
list(APPEND _test_targets connect_test)
#add_executable(multi_thread_test multi_thread_test.cpp ${DEPENDENT_SOURCE_DIR}/ThreadPool.cpp)
#target_link_libraries(multi_thread_test ${_linke_libs})
#list(APPEND _test_targets multi_thread_test)

add_executable(get_person_test get_person_test.cpp)
target_link_libraries(get_person_test ${_linke_libs})
list(APPEND _test_targets get_person_test)
################创建 erpc_compile_test 项目用于　windows　下编译测试#################
if(WIN32)
# 查找 thrift库
find_package(Thrift REQUIRED)

set(_erpc_proxy_dir "${CMAKE_CURRENT_LIST_DIR}/../erpc_proxy" )
set(_stub_dir "${CMAKE_CURRENT_LIST_DIR}/../client/stub") 
set(_erpc_infra_dir "${CMAKE_CURRENT_LIST_DIR}/../../dependencies/erpc/erpc/erpc_c/infra") 
set(_erpc_port_dir "${CMAKE_CURRENT_LIST_DIR}/../../dependencies/erpc/erpc/erpc_c/port") 
set(_erpc_config_dir "${CMAKE_CURRENT_LIST_DIR}/../../dependencies/erpc/erpc/erpc_c/config") 
set(_erpc_transports_dir "${CMAKE_CURRENT_LIST_DIR}/../../dependencies/erpc/erpc/erpc_c/transports") 
set(_erpc_setup_dir "${CMAKE_CURRENT_LIST_DIR}/../../dependencies/erpc/erpc/erpc_c/setup") 
set(_erpc_concurrency_dir "${CMAKE_CURRENT_LIST_DIR}/../../dependencies/erpc/concurrency") 

aux_source_directory(${_stub_dir} _STUB_SOURCES)
aux_source_directory(${_erpc_proxy_dir} _PROXY_SOURCES)
add_executable(erpc_compile_test erpc_client_test.cpp erpc_client_mt_test.cpp ${_STUB_SOURCES} ${_PROXY_SOURCES})
target_include_directories(erpc_compile_test PRIVATE ${_erpc_proxy_dir} ${_stub_dir} ${_erpc_infra_dir} 
	${_erpc_port_dir} 
	${_erpc_config_dir}
	${_erpc_transports_dir}
	${_erpc_setup_dir}
	${_erpc_concurrency_dir})
list(APPEND _link_targets common_source )
if(MSVC) 
	if(TARGET thrift_static_mt)
		with_mt_if_msvc(erpc_compile_test)
		list(APPEND _link_targets thrift_static_mt)
	endif(TARGET thrift_static_mt)
	if(TARGET thrift_static_md)
		list(APPEND _link_targets thrift_static_md )
	endif(TARGET thrift_static_md)
else()
	if(TARGET thrift_static)
        list(APPEND _link_targets thrift_static )
    elseif(TARGET thrift)
        list(APPEND _link_targets thrift )
	endif(TARGET thrift_static)
endif(MSVC)
target_link_libraries(erpc_compile_test ${_link_targets})
target_compile_definitions(erpc_compile_test 
			PRIVATE $<$<CXX_COMPILER_ID:MSVC>:NOMINMAX> 
	        PUBLIC _SL_USE_BYTE_STREAM 
					)
list(APPEND _test_targets erpc_compile_test)
endif()

if(UNIX)
if(NOT TARGET erpc)
find_package(Erpc MODULE REQUIRED)
endif()
set(_erpc_proxy_dir "${CMAKE_CURRENT_LIST_DIR}/../erpc_proxy" )
################创建 erpc_client_mt_test 项目用于 linux 下测试#################
add_executable(erpc_client_test erpc_client_test.cpp  
	${_erpc_proxy_dir}/facelog_client.cpp 
	${_erpc_proxy_dir}/facelog_erpc_output.cpp
	${_erpc_proxy_dir}/facelog_erpc_mem.cpp)
target_include_directories(erpc_client_test PRIVATE ${_erpc_proxy_dir} )
target_link_libraries(erpc_client_test erpc common_source) 
if(CMAKE_SYSTEM_NAME MATCHES "Linux")
	target_link_libraries(erpc_client_test -pthread)
endif()
list(APPEND _test_targets erpc_client_test)
################创建 erpc_client_mt_test 项目用于 linux 下测试#################
add_executable(erpc_client_mt_test erpc_client_mt_test.cpp  
	${_erpc_proxy_dir}/facelog_client.cpp 
	${_erpc_proxy_dir}/facelog_erpc_output.cpp
	${_erpc_proxy_dir}/facelog_erpc_mem.cpp
	${DEPENDENT_SOURCE_DIR}/ThreadPool.cpp)
target_include_directories(erpc_client_mt_test PRIVATE ${_erpc_proxy_dir} )
target_link_libraries(erpc_client_mt_test erpc common_source) 
if(CMAKE_SYSTEM_NAME MATCHES "Linux")
	target_link_libraries(erpc_client_mt_test -pthread)
endif()
list(APPEND _test_targets erpc_client_mt_test)
####################################################################
endif(UNIX)
##################################
set_target_properties(${_test_targets} PROPERTIES FOLDER "test")
set_property(TARGET ${_test_targets} APPEND PROPERTY COMPILE_OPTIONS $<$<CXX_COMPILER_ID:MSVC>:/wd4275 /wd4251>
	$<$<CXX_COMPILER_ID:GNU>:-Wno-format-security>)
################安装脚本#################
# 安装exe
install(TARGETS ${_test_targets} RUNTIME DESTINATION ${RUNTIME_INSTALL_DIR} )
