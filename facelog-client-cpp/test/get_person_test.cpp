/*
* get_person_test.cpp
* client连接测试
*  Created on: 2020年03月17日
*      Author: guyadong
*/
#include <iostream>
#include <nlohmann/json.hpp>
#include "sample_log.h"
#include "facelogclient/IFaceLogIfClient.h"
#include "BaseCmdParam.h"

using namespace net::gdface::facelog;
using namespace gdface;
using namespace codegen;
using namespace nlohmann;
using namespace std;

void test_getPerson(IFaceLogIfClient &client) {
	cout << "\ntest_getPerson\n" ;
	db::PersonBean person;
	int person_id = 1;
	auto p = client.getPerson(person,person_id);
	if (p) {
		person.printTo(cout);
		cout << endl;
		json j = person;
		cout << "person'sjson=" << j << endl;
	}
	else {
		cout << "not found person bean for " << person_id << endl;
	}
	
}

void test_loadPersonByWhere(IFaceLogIfClient &client) {
	cout << "\ntest_loadPersonByWhere\n" ;
	codegen::stl_wrapper<std::vector<net::gdface::facelog::db::PersonBean>> person_ids;
	int person_id = 1;
	auto p = client.loadPersonByWhere(person_ids,std::string(""),1,-1);
	if (p) {
		int c = 0;
		for (auto e : person_ids.get()) {
			cout << c << " person e: " << e << endl;
		}
	}
	else {
		cout << "not found person bean for " << person_id << endl;
	}
}

int main(int argc, char *argv[]) {
	BaseClientConfig param;
	param.parse(argc, argv);

	auto client = IFaceLogIfClient(param.host, param.port);

	try {
		SAMPLE_OUT("connect service:{}@{} ...", param.port, param.host);
		test_getPerson(client);
		test_loadPersonByWhere(client);
	}
	catch (std::exception& tx) {
		SAMPLE_ERR("ERROR: {}", tx.what());
	}
}