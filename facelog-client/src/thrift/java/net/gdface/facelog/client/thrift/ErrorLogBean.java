package net.gdface.facelog.client.thrift;

import com.facebook.swift.codec.*;
import com.facebook.swift.codec.ThriftField.Requiredness;
import java.util.*;

import static com.google.common.base.Objects.toStringHelper;

@ThriftStruct("ErrorLogBean")
public final class ErrorLogBean
{
    public ErrorLogBean() {
    }

    private boolean New;

    @ThriftField(value=1, name="_new", requiredness=Requiredness.REQUIRED)
    public boolean isNew() { return New; }

    @ThriftField
    public void setNew(final boolean New) { this.New = New; }

    private int modified;

    @ThriftField(value=2, name="modified", requiredness=Requiredness.REQUIRED)
    public int getModified() { return modified; }

    @ThriftField
    public void setModified(final int modified) { this.modified = modified; }

    private int initialized;

    @ThriftField(value=3, name="initialized", requiredness=Requiredness.REQUIRED)
    public int getInitialized() { return initialized; }

    @ThriftField
    public void setInitialized(final int initialized) { this.initialized = initialized; }

    private Integer id;

    @ThriftField(value=4, name="id", requiredness=Requiredness.OPTIONAL)
    public Integer getId() { return id; }

    @ThriftField
    public void setId(final Integer id) { this.id = id; }

    private String catalog;

    @ThriftField(value=5, name="catalog", requiredness=Requiredness.OPTIONAL)
    public String getCatalog() { return catalog; }

    @ThriftField
    public void setCatalog(final String catalog) { this.catalog = catalog; }

    private Integer personId;

    @ThriftField(value=6, name="personId", requiredness=Requiredness.OPTIONAL)
    public Integer getPersonId() { return personId; }

    @ThriftField
    public void setPersonId(final Integer personId) { this.personId = personId; }

    private Integer deviceId;

    @ThriftField(value=7, name="deviceId", requiredness=Requiredness.OPTIONAL)
    public Integer getDeviceId() { return deviceId; }

    @ThriftField
    public void setDeviceId(final Integer deviceId) { this.deviceId = deviceId; }

    private String exceptionClass;

    @ThriftField(value=8, name="exceptionClass", requiredness=Requiredness.OPTIONAL)
    public String getExceptionClass() { return exceptionClass; }

    @ThriftField
    public void setExceptionClass(final String exceptionClass) { this.exceptionClass = exceptionClass; }

    private String message;

    @ThriftField(value=9, name="message", requiredness=Requiredness.OPTIONAL)
    public String getMessage() { return message; }

    @ThriftField
    public void setMessage(final String message) { this.message = message; }

    private String stackTrace;

    @ThriftField(value=10, name="stackTrace", requiredness=Requiredness.OPTIONAL)
    public String getStackTrace() { return stackTrace; }

    @ThriftField
    public void setStackTrace(final String stackTrace) { this.stackTrace = stackTrace; }

    private Long createTime;

    @ThriftField(value=11, name="createTime", requiredness=Requiredness.OPTIONAL)
    public Long getCreateTime() { return createTime; }

    @ThriftField
    public void setCreateTime(final Long createTime) { this.createTime = createTime; }

    @Override
    public String toString()
    {
        return toStringHelper(this)
            .add("New", New)
            .add("modified", modified)
            .add("initialized", initialized)
            .add("id", id)
            .add("catalog", catalog)
            .add("personId", personId)
            .add("deviceId", deviceId)
            .add("exceptionClass", exceptionClass)
            .add("message", message)
            .add("stackTrace", stackTrace)
            .add("createTime", createTime)
            .toString();
    }
}
