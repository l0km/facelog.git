package net.gdface.facelog.client.thrift;

import com.facebook.swift.codec.*;
import com.facebook.swift.codec.ThriftField.Requiredness;
import java.util.*;

import static com.google.common.base.Objects.toStringHelper;

@ThriftStruct("LockWakeupResponse")
public final class LockWakeupResponse
{
    public LockWakeupResponse() {
    }

    private DeviceBean deviceBean;

    @ThriftField(value=1, name="deviceBean", requiredness=Requiredness.OPTIONAL)
    public DeviceBean getDeviceBean() { return deviceBean; }

    @ThriftField
    public void setDeviceBean(final DeviceBean deviceBean) { this.deviceBean = deviceBean; }

    private String iso8601Timestamp;

    @ThriftField(value=2, name="iso8601Timestamp", requiredness=Requiredness.OPTIONAL)
    public String getIso8601Timestamp() { return iso8601Timestamp; }

    @ThriftField
    public void setIso8601Timestamp(final String iso8601Timestamp) { this.iso8601Timestamp = iso8601Timestamp; }

    private List<PersonSummary> persons;

    @ThriftField(value=3, name="persons", requiredness=Requiredness.OPTIONAL)
    public List<PersonSummary> getPersons() { return persons; }

    @ThriftField
    public void setPersons(final List<PersonSummary> persons) { this.persons = persons; }

    private List<TmpPassword> tmpPasswords;

    @ThriftField(value=4, name="tmpPasswords", requiredness=Requiredness.OPTIONAL)
    public List<TmpPassword> getTmpPasswords() { return tmpPasswords; }

    @ThriftField
    public void setTmpPasswords(final List<TmpPassword> tmpPasswords) { this.tmpPasswords = tmpPasswords; }

    private Token token;

    @ThriftField(value=5, name="token", requiredness=Requiredness.OPTIONAL)
    public Token getToken() { return token; }

    @ThriftField
    public void setToken(final Token token) { this.token = token; }

    @Override
    public String toString()
    {
        return toStringHelper(this)
            .add("deviceBean", deviceBean)
            .add("iso8601Timestamp", iso8601Timestamp)
            .add("persons", persons)
            .add("tmpPasswords", tmpPasswords)
            .add("token", token)
            .toString();
    }
}
