package net.gdface.facelog.client.thrift;

import com.facebook.swift.codec.*;
import com.facebook.swift.codec.ThriftField.Requiredness;
import java.util.*;

import static com.google.common.base.Objects.toStringHelper;

@ThriftStruct("TopGroupInfo")
public final class TopGroupInfo
{
    public TopGroupInfo() {
    }

    private boolean cleanExistingTop;

    @ThriftField(value=1, name="cleanExistingTop", requiredness=Requiredness.REQUIRED)
    public boolean isCleanExistingTop() { return cleanExistingTop; }

    @ThriftField
    public void setCleanExistingTop(final boolean cleanExistingTop) { this.cleanExistingTop = cleanExistingTop; }

    private boolean clearMore;

    @ThriftField(value=2, name="clearMore", requiredness=Requiredness.REQUIRED)
    public boolean isClearMore() { return clearMore; }

    @ThriftField
    public void setClearMore(final boolean clearMore) { this.clearMore = clearMore; }

    private boolean deleteElement;

    @ThriftField(value=3, name="deleteElement", requiredness=Requiredness.REQUIRED)
    public boolean isDeleteElement() { return deleteElement; }

    @ThriftField
    public void setDeleteElement(final boolean deleteElement) { this.deleteElement = deleteElement; }

    private String name;

    @ThriftField(value=4, name="name", requiredness=Requiredness.OPTIONAL)
    public String getName() { return name; }

    @ThriftField
    public void setName(final String name) { this.name = name; }

    private Map<String, String> nodes;

    @ThriftField(value=5, name="nodes", requiredness=Requiredness.OPTIONAL)
    public Map<String, String> getNodes() { return nodes; }

    @ThriftField
    public void setNodes(final Map<String, String> nodes) { this.nodes = nodes; }

    private Map<String, String> permits;

    @ThriftField(value=6, name="permits", requiredness=Requiredness.OPTIONAL)
    public Map<String, String> getPermits() { return permits; }

    @ThriftField
    public void setPermits(final Map<String, String> permits) { this.permits = permits; }

    @Override
    public String toString()
    {
        return toStringHelper(this)
            .add("cleanExistingTop", cleanExistingTop)
            .add("clearMore", clearMore)
            .add("deleteElement", deleteElement)
            .add("name", name)
            .add("nodes", nodes)
            .add("permits", permits)
            .toString();
    }
}
