package net.gdface.facelog;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.google.common.base.Preconditions.*;
public class JdkVersion {

    private static final String javaVersion;  
    private static final int version;
  
    static {  
        javaVersion = System.getProperty("java.version");  
        Pattern pattern = Pattern.compile("1\\.(\\d+)");
        Matcher matcher = pattern.matcher(javaVersion);
        checkState(matcher.find(),"NOT MATCH java version %s",javaVersion);
        version  = Integer.valueOf(matcher.group(1)).intValue();  
    }  
  
    public static String getJavaVersion() {  
        return javaVersion;  
    }  
  
    public static boolean isJava8() {  
        return version == 8; 
    }

	public static int getVersion() {
		return version;
	} 
}
