package net.gdface.facelog;

import static com.google.common.base.Preconditions.*;

import gu.simplemq.IMessageQueueFactory;
import gu.simplemq.IPublisher;
import gu.simplemq.MessageQueueFactorys;
import gu.sql2java.TableListener;
import gu.sql2java.exception.RuntimeDaoException;
import net.gdface.facelog.db.DeviceBean;

/**
 * 设备表({@code fl_device})变动侦听器<br>
 * 当{@code fl_device}记录增删改时发布订阅消息
 * @author guyadong
 *
 */
class MQDeviceListener extends TableListener.Adapter<DeviceBean> implements ChannelConstant{

	private final IPublisher publisher;
	private final BaseDao dao;
	private DeviceBean beforeUpdatedBean;
	public MQDeviceListener(BaseDao dao) {
		this(dao, MessageQueueFactorys.getDefaultFactory());
	}
	public MQDeviceListener(BaseDao dao, IMessageQueueFactory factory) {
		this.dao = checkNotNull(dao,"dao is null");
		this.publisher = checkNotNull(factory,"factory is null").getPublisher();
	}
	@Override
	public void afterInsert(DeviceBean bean) {
		new PublishTask<Integer>(
				PUBSUB_DEVICE_INSERT, 
				bean.getId(), 
				publisher)
		.execute();
	}
	@Override
	public void beforeUpdate(DeviceBean bean) throws RuntimeDaoException {
		// 保留更新前的数据
		beforeUpdatedBean = dao.daoGetDevice(bean.getId()).clone();
	}
	@Override
	public void afterUpdate(DeviceBean bean) {
		// beforeUpdatedBean 为 null，只可能因为侦听器是被异步调用的
		checkState(beforeUpdatedBean != null,"beforeUpdatedBean must not be null");
		// 保存修改信息
		beforeUpdatedBean.setModified(bean.getModified());
		new PublishTask<DeviceBean>(
				PUBSUB_DEVICE_UPDATE, 
				beforeUpdatedBean, 
				publisher)
		.execute();
		beforeUpdatedBean = null;
	}

	@Override
	public void afterDelete(DeviceBean bean) {
		new PublishTask<DeviceBean>(
				PUBSUB_DEVICE_DELETE, 
				bean, 
				publisher)
		.execute();
	}			

}
