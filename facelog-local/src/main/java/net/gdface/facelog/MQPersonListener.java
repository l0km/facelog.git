package net.gdface.facelog;

import static com.google.common.base.Preconditions.*;

import gu.simplemq.IMessageQueueFactory;
import gu.simplemq.IPublisher;
import gu.simplemq.MessageQueueFactorys;
import gu.sql2java.TableListener;
import gu.sql2java.exception.RuntimeDaoException;
import net.gdface.facelog.db.PersonBean;

/**
 * 人员表({@code fl_person})变动侦听器<br>
 * 当{@code fl_person}记录增删改时发布订阅消息
 * @author guyadong
 *
 */
class MQPersonListener extends TableListener.Adapter<PersonBean> implements ChannelConstant{

	private final IPublisher publisher;
	private final BaseDao dao;
	private PersonBean beforeUpdatedBean;
	public MQPersonListener(BaseDao dao) {
		this(dao, MessageQueueFactorys.getDefaultFactory());
	}
	public MQPersonListener(BaseDao dao, IMessageQueueFactory factory) {
		this.dao = checkNotNull(dao,"dao is null");
		this.publisher = checkNotNull(factory,"factory is null").getPublisher();
	}
	@Override
	public void afterInsert(PersonBean bean) {
		new PublishTask<Integer>(
				PUBSUB_PERSON_INSERT, 
				bean.getId(), 
				publisher)
		.execute();
	}
	@Override
	public void beforeUpdate(PersonBean bean) throws RuntimeDaoException {
		// 保留更新前的数据
		beforeUpdatedBean = dao.daoGetPersonChecked(bean.getId()).clone();
	}
	@Override
	public void afterUpdate(PersonBean bean) {
		// beforeUpdatedBean 为 null，只可能因为侦听器是被异步调用的
		checkState(beforeUpdatedBean != null,"beforeUpdatedBean must not be null");
		// 保存修改信息
		beforeUpdatedBean.setModified(bean.getModified());
		new PublishTask<PersonBean>(
				PUBSUB_PERSON_UPDATE, 
				beforeUpdatedBean, 
				publisher)
		.execute();
		beforeUpdatedBean = null;
	}

	@Override
	public void afterDelete(PersonBean bean) {
		new PublishTask<PersonBean>(
				PUBSUB_PERSON_DELETE, 
				bean, 
				publisher)
		.execute();
	}			

}
