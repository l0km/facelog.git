package net.gdface.facelog;

import static gu.sql2java.SimpleLog.*;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import gu.sql2java.BaseBean;
import gu.sql2java.Managers;
import gu.sql2java.TableManager;

public class TestCache {


	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		TableManagerInitializer.INSTANCE.init();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void test() {
		// 显示所有的native manager
		log("Native Managers:");
		for(TableManager<? extends BaseBean> manager:Managers.getTableManagers().values()){
			logObjects(manager);
		}
		// 显示所有加载的cache manager
		log("Cache Managers:");
		for(TableManager<? extends BaseBean> manager:Managers.getCacheManagers().values()){
			logObjects(manager);	
		}
		// 显示当前工作状态的table manager
		log("Working Managers:");
		for(String tablename:Managers.getTableManagers().keySet()){
			logObjects(Managers.managerOf(tablename));	
		}
	}

}
