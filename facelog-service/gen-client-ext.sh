#!/bin/bash
sh_folder=$(cd "$(dirname $0)"; pwd -P)
pushd "$sh_folder"
./gen-client-c.sh || exit 
./gen-client-csharp.sh || exit 
./gen-client-jquery.sh || exit 
./gen-client-node.js.sh || exit 
popd
