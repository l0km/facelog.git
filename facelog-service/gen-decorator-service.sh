#!/bin/bash
# 生成接口的基于facebook/swift的service端实现代码(decorator)
sh_folder=$(cd "$(dirname $0)"; pwd -P)
pushd $sh_folder
OUT_FOLDER=src/codegen/java
[ -e "$OUT_FOLDER" ] && rm -fr "$OUT_FOLDER "
mvn com.gitee.l0km:codegen-thrift-maven-plugin:generate || exit
popd