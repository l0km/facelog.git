#!/bin/bash
sh_folder=$(cd "$(dirname $0)"; pwd -P)
pushd $sh_folder/../facelog-client
OUT_FOLDER=src/thrift/java
[ -e "$OUT_FOLDER" ] && rm -fr "$OUT_FOLDER"
mvn com.facebook.mojo:swift-maven-plugin:generate || exit
popd