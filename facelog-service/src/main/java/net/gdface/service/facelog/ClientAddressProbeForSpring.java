package net.gdface.service.facelog;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import net.gdface.facelog.FacelogDecorator.BaseClientAddressProbe;

/**
 * 用于Spring的客户端地址侦听器
 * @author guyadong
 *
 */
class ClientAddressProbeForSpring extends BaseClientAddressProbe {
	static ClientAddressProbeForSpring SPRING_ADDRESS_PROBE = new ClientAddressProbeForSpring(); 
	ClientAddressProbeForSpring() {
	}

	public static final String niftyClientAddressAsString(){
		RequestAttributes requestAttributes = RequestContextHolder.currentRequestAttributes();
		if (requestAttributes instanceof ServletRequestAttributes){
			return ((ServletRequestAttributes)requestAttributes).getRequest().getRemoteAddr();
		}
		return null;
	}
	
	@Override
	protected String getClientAddress() {
		return niftyClientAddressAsString();
	}

}
